package br.inf.efaber;

import java.util.Calendar;
import java.util.Date;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {

  /**
   * Create the test case
   * @param testName 
   * name of the test case
   */
  public AppTest(final String testName) {
	super(testName);
  }
  
  public static void main(final String[] args) {
	final Calendar cal = Calendar.getInstance();
	cal.set(Calendar.HOUR_OF_DAY, 4);
	cal.set(Calendar.MINUTE, 32);
	final Date dt1 = cal.getTime();
	cal.set(Calendar.HOUR_OF_DAY, 7);
	cal.set(Calendar.MINUTE, 1);
	final Date dt2 = cal.getTime();
  }
  
  /**
   * @return the suite of tests being tested
   */
  public static Test suite() {
	return new TestSuite(AppTest.class);
  }

  /**
   * Rigourous Test :-)
   */
  public void testApp() {
	assertTrue(true);
  }
}
