/**
 * (c)2012 EFaber Consultoria Ltda.
 * Direitos reservados
 */
package br.com.rdigit.deskjira.persistence;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author leopoldo.barreiro
 * @since 16/12/2012 20:03:26
 */
@ApplicationScoped
public class EntityManagerProducer {

  private EntityManagerFactory entityManagerFactory;
  private EntityManager entityManager;

  public EntityManagerProducer() {
	entityManagerFactory = Persistence.createEntityManagerFactory("efaberPersistenceUnit");
	entityManager = entityManagerFactory.createEntityManager();
	// entityManager.setFlushMode(FlushModeType.AUTO);
  }

  @Produces
  @ApplicationScoped
  public EntityManager getEntityManager() {
	return entityManager;
  }

}
