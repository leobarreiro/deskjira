package br.com.rdigit.deskjira;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Tray;
import org.eclipse.swt.widgets.TrayItem;
import org.jboss.weld.environment.se.events.ContainerInitialized;

import br.com.rdigit.deskjira.entity.User;
import br.com.rdigit.deskjira.screen.UserScreen;
import br.com.rdigit.deskjira.util.TimeUtil;

/**
 * @author leopoldo.barreiro
 * @since 11/11/2012 16:50:39
 */
@ApplicationScoped
public class App {

	private static final String TRAYICON_IMAGE = "/img/clock16.png";

	@Inject
	@Named("defaultDisplay")
	private Display display;

	@Inject
	private EntityManager entityManager;

	@Inject
	private UserScreen userScreen;

	@Inject
	@Named("currentUser")
	private User user;

	@Inject
	@Named("deskJiraMenu")
	private Menu menu;

	public void main(@Observes ContainerInitialized event) { // NOPMD comportamento CDI Weld nao interpretado corretamente pelo PMD
		try {
			// System Tray
			final Tray tray = display.getSystemTray();
			if (tray != null) {
				final TrayItem item = new TrayItem(tray, SWT.NONE);
				item.setImage(new Image(display, App.class.getResourceAsStream(TRAYICON_IMAGE)));
				item.addSelectionListener(new SelectionListener() {
					public void widgetSelected(final SelectionEvent arg0) {
						menu.setVisible(Boolean.TRUE);
					}

					public void widgetDefaultSelected(SelectionEvent arg0) {
						menu.setVisible(Boolean.TRUE);
					}
				});
				item.addListener(SWT.MenuDetect, new Listener() {
					public void handleEvent(Event event) {
						menu.setVisible(true);
					}
				});

				if (user.getJiraUrl() == null || user.getJiraUrl().isEmpty()) {
					userScreen.open();
					userScreen.layout();
					userScreen.initScreen();
				}

			}
			while (!menu.isDisposed()) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}
			entityManager.close();
			System.exit(0); // NOPMD saida padrao do sistema
		} catch (Exception e) {
			TimeUtil.handleError(e);
			e.printStackTrace();
		}
	}

}
