/**
 * (c)2012 EFaber Consultoria Ltda.
 * Direitos reservados
 */
package br.com.rdigit.deskjira.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author leopoldo.barreiro
 */
@Entity
@Table(name = "user")
@SequenceGenerator(name = "sq_usr", sequenceName = "seq_user")
public class User implements IEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5524347566065243171L;
	private Long id;
	private String username;
	private String name;
	private String email;
	private String language;
	private String jiraUrl;
	private String jiraUser;
	private String jiraPassword;
	private String jiraVersion;
	private byte[] avatar;

	/**
	 * @return the id
	 */
	@Id
	@Column(name = "user_id")
	@GeneratedValue(generator = "sq_usr", strategy = GenerationType.SEQUENCE)
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the username
	 */
	@Column(name = "username", length = 40, nullable = false)
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the name
	 */
	@Column(name = "name", length = 120, nullable = true)
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the email
	 */
	@Column(name = "email", length = 120, nullable = true)
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the language
	 */
	@Column(name = "language", length = 5, nullable = true)
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language
	 *            the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the jiraUser
	 */
	@Column(name = "jira_user", length = 120, nullable = true)
	public String getJiraUser() {
		return jiraUser;
	}

	/**
	 * @param jiraUser
	 *            the jiraUser to set
	 */
	public void setJiraUser(String jiraUser) {
		this.jiraUser = jiraUser;
	}

	/**
	 * @return the jiraPassword
	 */
	@Column(name = "jira_pwd", length = 120, nullable = true)
	public String getJiraPassword() {
		return jiraPassword;
	}

	/**
	 * @param jiraPassword
	 *            the jiraPassword to set
	 */
	public void setJiraPassword(String jiraPassword) {
		this.jiraPassword = jiraPassword;
	}

	/**
	 * @return the jiraUrl
	 */
	@Column(name = "jira_url", length = 250, nullable = true)
	public String getJiraUrl() {
		return jiraUrl;
	}

	/**
	 * @param jiraUrl
	 *            the jiraUrl to set
	 */
	public void setJiraUrl(String jiraUrl) {
		this.jiraUrl = jiraUrl;
	}

	/**
	 * @return the jiraVersion
	 */
	@Column(name = "jira_version", length = 30, nullable = true)
	public String getJiraVersion() {
		return jiraVersion;
	}

	/**
	 * @param jiraVersion
	 *            the jiraVersion to set
	 */
	public void setJiraVersion(String jiraVersion) {
		this.jiraVersion = jiraVersion;
	}

	/**
	 * @return the avatar
	 */
	@Transient
	public byte[] getAvatar() {
		return avatar;
	}

	/**
	 * @param avatar
	 *            the avatar to set
	 */
	public void setAvatar(byte[] avatar) {
		this.avatar = avatar;
	}

}
