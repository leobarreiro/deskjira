/**
 * 
 */
package br.com.rdigit.deskjira.entity;


/**
 * @author leopoldo.barreiro
 * 
 */
public class Issue implements IEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 861951412869175171L;
	private Long id;
	private String issueKey;
	private String summary;
	private String description;
	private String status;
	private String resolution;
	private IssueType issueType;

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.inf.efaber.deskjira.entity.IEntity#getId()
	 */
	public Long getId() {
		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.inf.efaber.deskjira.entity.IEntity#setId(java.lang.Long)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the issueKey
	 */
	public String getIssueKey() {
		return issueKey;
	}

	/**
	 * @param issueKey
	 *            the issueKey to set
	 */
	public void setIssueKey(String issueKey) {
		this.issueKey = issueKey;
	}

	/**
	 * @return the summary
	 */
	public String getSummary() {
		return summary;
	}

	/**
	 * @param summary
	 *            the summary to set
	 */
	public void setSummary(String summary) {
		this.summary = summary;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the resolution
	 */
	public String getResolution() {
		return resolution;
	}

	/**
	 * @param resolution
	 *            the resolution to set
	 */
	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	/**
	 * @return the issueType
	 */
	public IssueType getIssueType() {
		return issueType;
	}

	/**
	 * @param issueType
	 *            the issueType to set
	 */
	public void setIssueType(IssueType issueType) {
		this.issueType = issueType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(this.getIssueKey());
		sb.append("] ");
		sb.append(this.getSummary());
		sb.append("\n");
		sb.append(this.getDescription());
		return sb.toString();
	}

}
