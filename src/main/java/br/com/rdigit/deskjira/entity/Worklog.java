/**
 * (c)2012 EFaber Consultoria Ltda.
 * Direitos reservados
 */
package br.com.rdigit.deskjira.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * @author leopoldo.barreiro
 */
@Entity
@Table(name = "worklog")
public class Worklog implements IEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8345127370349460131L;
	private Long id;
	private User user;
	private String issueKey;
	private String summary;
	private Date startDateTime;
	private String worked;
	private String comment;

	@Id
	@Column(name = "worklog_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(targetEntity = User.class)
	@JoinColumn(name = "user_id")
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the issueKey
	 */
	@Column(name = "issue_key", length = 20, nullable = false)
	public String getIssueKey() {
		return issueKey;
	}

	/**
	 * @param issueKey
	 *            the issueKey to set
	 */
	public void setIssueKey(String issueKey) {
		this.issueKey = issueKey;
	}

	/**
     * @return the summary
     */
    @Transient
	public String getSummary() {
        return summary;
    }

    /**
     * @param summary the summary to set
     */
    public void setSummary(String summary) {
        this.summary = summary;
    }

    /**
	 * @return the startDateTime
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "started")
	public Date getStartDateTime() {
		return startDateTime;
	}

	/**
	 * @param startDateTime
	 *            the startDateTime to set
	 */
	public void setStartDateTime(Date startDateTime) {
		this.startDateTime = startDateTime;
	}

	/**
	 * @return the worked
	 */
	@Column(name = "worked", length = 30, nullable = false)
	public String getWorked() {
		return worked;
	}

	/**
	 * @param worked
	 *            the worked to set
	 */
	public void setWorked(String worked) {
		this.worked = worked;
	}

	/**
	 * @return the comment
	 */
	@Column(name = "comment", length = 300, nullable = true)
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment
	 *            the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	

}
