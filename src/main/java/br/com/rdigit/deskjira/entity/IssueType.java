/**
 * 
 */
package br.com.rdigit.deskjira.entity;

/**
 * @author leopoldo.barreiro
 * 
 */
public class IssueType implements IEntity {

	private static final long serialVersionUID = 746124431848373882L;
	private Long id;
	private String name;
	private String iconUrl;
	private String description;

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.inf.efaber.deskjira.entity.IEntity#getId()
	 */
	public Long getId() {
		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.inf.efaber.deskjira.entity.IEntity#setId(java.lang.Long)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the iconUrl
	 */
	public String getIconUrl() {
		return iconUrl;
	}

	/**
	 * @param iconUrl
	 *            the iconUrl to set
	 */
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
