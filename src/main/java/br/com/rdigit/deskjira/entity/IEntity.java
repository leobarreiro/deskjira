/**
 * 
 */
package br.com.rdigit.deskjira.entity;

import java.io.Serializable;

/**
 * @author leopoldo.barreiro
 *
 */
public interface IEntity extends Serializable {
	
	Long getId();
	
	void setId(Long id);

}
