/**
 * 
 */
package br.com.rdigit.deskjira.entity;

import java.io.Serializable;

/**
 * @author leopoldo.barreiro
 * 
 */
public class RestResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6233727957275800404L;

	private Integer code;
	private String reason;
	private String content;

	/**
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * @param reason
	 *            the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content
	 *            the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

}
