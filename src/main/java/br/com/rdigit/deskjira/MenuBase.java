/**
 * (c)2013 EFaber Consultoria Ltda.
 * Direitos reservados
 */
package br.com.rdigit.deskjira;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;

import br.com.rdigit.deskjira.business.IIssueBusiness;
import br.com.rdigit.deskjira.business.IUserBusiness;
import br.com.rdigit.deskjira.business.IWorklogBusiness;
import br.com.rdigit.deskjira.entity.Issue;
import br.com.rdigit.deskjira.entity.User;
import br.com.rdigit.deskjira.screen.UserScreen;
import br.com.rdigit.deskjira.screen.WorkLogReportScreen;
import br.com.rdigit.deskjira.screen.WorkLogScreen;
import br.com.rdigit.deskjira.util.Props;
import br.com.rdigit.deskjira.util.ResourceUtil;

@Named
@ApplicationScoped
public class MenuBase implements Serializable {

	private static final long serialVersionUID = -8808314693400375442L;

	@Inject
	private WorkLogScreen worklogScreen;

	@Inject
	private UserScreen userScreen;

	@Inject
	private WorkLogReportScreen worklogReportScreen;

	@Inject
	@Named("defaultDisplay")
	private Display display;

	@Inject
	private IIssueBusiness issueBusiness;

	@Inject
	private IWorklogBusiness worklogBusiness;

	@Inject
	private IUserBusiness userBusiness;

	@Inject
	@Named("currentUser")
	private User user;

	private List<Issue> issues;

	@Inject
	private Props prp;

	private Menu menu;

	public MenuBase() {
	}

	public void createMenu() {
		menu = new Menu(worklogScreen, SWT.PUSH);
	}

	public void cleanMenuItems() {
		for (MenuItem mItem : menu.getItems()) {
			mItem.dispose();
		}
	}

	public void loadMenuItems() {
		if (userBusiness.isCurrentUserValidToJira()) {
			issues = issueBusiness.listAll(user);
			if (issues != null) {
				worklogScreen.setIssues(issues);
			}
		}
		// Work Log
		MenuItem mnWorklog = new MenuItem(menu, SWT.PUSH);
		mnWorklog.setData(ResourceUtil.DATA_SIGNATURE, ResourceUtil.DATA_CONTENT);
		mnWorklog.setText(prp.get("traymenu.worklog"));
		mnWorklog.setImage(new Image(display, App.class.getResourceAsStream(ResourceUtil.MENU_ICON_WORKLOGS)));
		mnWorklog.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent arg0) {
				openWorklogScreen();
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				openWorklogScreen();
			}
		});

		// Work Log Report

		MenuItem mnReport = new MenuItem(menu, SWT.PUSH);
		mnReport.setText(prp.get("traymenu.worklog.report"));
		mnReport.setImage(new Image(display, App.class.getResourceAsStream(ResourceUtil.MENU_ICON_REPORT)));
		mnReport.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent arg0) {
				openWorklogReportScreen();
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				openWorklogReportScreen();
			}
		});

		// User Options

		MenuItem mnUserOpt = new MenuItem(menu, SWT.PUSH);
		mnUserOpt.setData(ResourceUtil.DATA_SIGNATURE, ResourceUtil.DATA_CONTENT);
		mnUserOpt.setText(prp.get("traymenu.useropt"));
		mnUserOpt.setImage(new Image(display, App.class.getResourceAsStream(ResourceUtil.MENU_ICON_USER_OPTS)));
		mnUserOpt.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent arg0) {
				openUserScreen();
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				openUserScreen();
			}
		});

		MenuItem mnSep0 = new MenuItem(menu, SWT.SEPARATOR);
		mnSep0.setData(ResourceUtil.DATA_SIGNATURE, ResourceUtil.DATA_CONTENT);

		// Quit
		MenuItem mnQuit = new MenuItem(menu, SWT.PUSH);
		mnQuit.setData(ResourceUtil.DATA_SIGNATURE, ResourceUtil.DATA_CONTENT);
		mnQuit.setText(prp.get("traymenu.quit"));
		mnQuit.setImage(new Image(display, App.class.getResourceAsStream(ResourceUtil.MENU_ICON_QUIT)));
		mnQuit.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent arg0) {
				doAction();
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				doAction();
			}

			private void doAction() {
				menu.dispose();
				// System.exit(0);
			}
		});
	}

	public void openWorklogReportScreen() {
		worklogReportScreen.open();
		worklogReportScreen.layout();
		worklogReportScreen.initScreen();
	}

	public void openWorklogScreen() {
		worklogScreen.open();
		worklogScreen.layout();
		worklogScreen.initScreen();
	}

	public void openUserScreen() {
		userScreen.open();
		userScreen.layout();
		userScreen.initScreen();
	}

	@Produces
	@Named("deskJiraMenu")
	public Menu getMenu() {
		if (menu == null) {
			createMenu();
			loadMenuItems();
		}
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

}
