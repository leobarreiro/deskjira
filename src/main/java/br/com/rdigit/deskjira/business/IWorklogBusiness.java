/**
 * (c)2012 EFaber Consultoria Ltda.
 * Direitos reservados
 */
package br.com.rdigit.deskjira.business;

import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import br.com.rdigit.deskjira.entity.RestResponse;
import br.com.rdigit.deskjira.entity.User;
import br.com.rdigit.deskjira.entity.Worklog;

/**
 * @author leopoldo.barreiro
 * @since 18/11/2012 18:30:01
 */
@ApplicationScoped
public interface IWorklogBusiness extends IBusiness<Worklog> {

	/**
	 * Recupera uma lista de worklogs para o usuário indicado, no intervalo de
	 * tempo definido entre a data inicial e a data final
	 * 
	 * @param user
	 * @param startDate
	 * @param endDate
	 * @return worklogs
	 * @throws BusinessException
	 */
	List<Worklog> getWorklogsByDateInterval(User user, Date startDate, Date endDate) throws BusinessException;

	/**
	 * Adiciona um worklog.
	 * 
	 * @param worklog
	 * @return restResponse RestResponse
	 * @throws BusinessException
	 */
	RestResponse addWorkLog(Worklog worklog) throws BusinessException;

}
