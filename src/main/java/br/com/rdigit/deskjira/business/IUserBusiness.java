/**
 * (c)2012 EFaber Consultoria Ltda.
 * Direitos reservados
 */
package br.com.rdigit.deskjira.business;

import javax.enterprise.context.ApplicationScoped;

import br.com.rdigit.deskjira.entity.User;

/**
 * @author leopoldo.barreiro
 * @since 11/11/2012 18:19:22
 */
@ApplicationScoped
public interface IUserBusiness extends IBusiness<User> {

	public enum AvatarSize {
		XSMALL("16x16", 16, 16), SMALL("24x24", 24, 24), MEDIUM("32x32", 32, 32), LARGE("48x48", 48, 48);

		public String size;
		public int width;
		public int height;

		private AvatarSize(String size, int width, int height) {
			this.size = size;
			this.width = width;
			this.height = height;
		}
	}

	public enum Language {
		enUS("english (en-US)", "en-US"), ptBR("portugu\u00eas (pt-BR)", "pt-BR"), esES("espa\u00f1ol (es-ES)", "es-ES");

		public String label;
		public String lang;

		private Language(String label, String lang) {
			this.label = label;
			this.lang = lang;
		}

		public static Language getLanguageByLabel(String label) {
			Language language = null;
			for (Language l : Language.values()) {
				if (l.label.equals(label)) {
					language = l;
				}
			}
			return language;
		}

		public static Language getLanguageByLang(String lang) {
			Language language = null;
			for (Language l : Language.values()) {
				if (l.lang.equals(lang)) {
					language = l;
				}
			}
			return language;
		}

		public static String[] getLabels() {
			String[] labels = { Language.enUS.label, Language.esES.label, Language.ptBR.label };
			return labels;
		}
	};

	/**
	 * Recupera um usuario pesquisando pelo seu nome de usuario.
	 * 
	 * @param userName
	 *            String
	 * @return user User
	 */
	User getUserByUserName(String userName);

	/**
	 * Avalia se o usuario atual está com dados completos para efetuar um login nos serviços do Jira.
	 * 
	 * @return boolean isValid
	 */
	Boolean isCurrentUserValidToJira();

	/**
	 * Verifica se um usuario e valido para o Jira, ou seja, se possui os dados necessarios para autenticar-se no Jira. Retorna True se o usuario e valido,
	 * False do contrario.
	 * 
	 * @param user
	 * @return Boolean isValid
	 */
	Boolean isUserValidToJira(User user);

	/**
	 * Verifica as credenciais do usuario no Jira. Verifica a URL do Jira, o nome de usuario e senha. Caso o usuario seja corretamente autenticado, retorna
	 * True. Caso contrario, retorna False.
	 * 
	 * @param user
	 * @return boolean verified
	 */
	boolean isUserVerifiedInJira(User user);

	/**
	 * Avalia a label usada para selecao de linguagem do usuario e retorna o idioma correspondente, no padrao internacional.
	 * 
	 * @param label
	 * @return language
	 */
	String getLanguageByLabel(String label);

	/**
	 * Recupera o icone representativo do usuario no servidor Jira.
	 * 
	 * @param user
	 * @param avatarSize
	 * @return userIcon
	 */
	byte[] getUserIcon(User user, AvatarSize avatarSize);

}
