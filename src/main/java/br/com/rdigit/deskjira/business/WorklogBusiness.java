package br.com.rdigit.deskjira.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.Query;

import org.apache.http.entity.StringEntity;
import org.jboss.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.rdigit.deskjira.entity.RestResponse;
import br.com.rdigit.deskjira.entity.User;
import br.com.rdigit.deskjira.entity.Worklog;
import br.com.rdigit.deskjira.util.Props;
import br.com.rdigit.deskjira.util.StringUtil;
import br.com.rdigit.deskjira.util.TimeUtil;

/**
 * @author leopoldo.barreiro
 * @since 18/11/2012 18:33:20
 */
@ApplicationScoped
public class WorklogBusiness extends AbstractBusiness<Worklog> implements IWorklogBusiness {

	public static String MICROSECS = ":00.000-0300";

	@Inject
	private Props prp;

	public WorklogBusiness() {
		super();
		log = Logger.getLogger(this.getClass());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.inf.efaber.deskjira.business.IWorklogBusiness#getWorklogsByDateInterval (br.inf.efaber.deskjira.entity.User, java.util.Date, java.util.Date)
	 */
	public List<Worklog> getWorklogsByDateInterval(User user, Date startDate, Date endDate) throws BusinessException {
		List<Worklog> worklogList = new ArrayList<Worklog>(0);
		StringBuilder sb = new StringBuilder();
		sb.append(user.getJiraUrl());
		sb.append("/rest/timesheet-gadget/1.0/raw-timesheet.json?");
		sb.append("startDate=");
		sb.append(TimeUtil.dateToString(startDate, "yyyy-MM-dd"));
		sb.append("&endDate=");
		sb.append(TimeUtil.dateToString(endDate, "yyyy-MM-dd"));
		RestResponse restResponse = executeGetRequest(user, sb.toString());
		if (restResponse.getCode().equals(200)) {
			JSONObject json = new JSONObject();
			Worklog wl;
			try {
				json = new JSONObject(restResponse.getContent());
				if (json.length() > 0) {
					JSONArray jsonContent = json.getJSONArray("worklog");
					JSONObject jsonIssueLog;
					JSONArray jsonEntries;
					JSONObject jsonWorklog;
					for (int i = 0; i < jsonContent.length(); i++) {
						jsonIssueLog = jsonContent.getJSONObject(i);
						String key = jsonIssueLog.getString("key");
						String summary = jsonIssueLog.getString("summary");
						jsonEntries = jsonIssueLog.getJSONArray("entries");
						for (int f = 0; f < jsonEntries.length(); f++) {
							jsonWorklog = jsonEntries.getJSONObject(f);
							wl = convertJsonToWorklog(jsonWorklog, key, summary);
							worklogList.add(wl);
						}
					}
				}
			} catch (JSONException e) {
				log.error(e);
			}
		} else {
			throw new BusinessException(restResponse);
		}
		return worklogList;
	}

	@SuppressWarnings("unchecked")
	public List<Worklog> listAll(User user) {
		Query query = entityManager.createQuery("SELECT wl FROM Worklog wl WHERE wl.user.id = :idUser ORDER BY wl.logDate");
		query.setParameter("idUser", user.getId());
		return query.getResultList();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.inf.efaber.deskjira.business.IWorklogBusiness#addWorkLog(br.inf.efaber .deskjira.entity.Worklog)
	 */
	public RestResponse addWorkLog(Worklog worklog) {
		StringBuilder sb = new StringBuilder();
		RestResponse restResponse = null;
		if (userBusiness.isCurrentUserValidToJira()) {
			sb.append(user.getJiraUrl());
			sb.append("/rest/api/2/issue/");
			sb.append(worklog.getIssueKey());
			sb.append("/worklog?adjustEstimate=AUTO");
			JSONObject jsonObject = new JSONObject();
			try {
				jsonObject.put("timeSpent", TimeUtil.convertTimeToJiraFormat(worklog.getWorked()));
				jsonObject.put("comment", StringUtil.stripAccents(worklog.getComment()));
				jsonObject.put("started", TimeUtil.dateToString(worklog.getStartDateTime(), "yyyy-MM-dd") + "T" + TimeUtil.dateToString(worklog.getStartDateTime(), "HH:mm") + MICROSECS);
				StringEntity stringEntity;
				System.out.println(jsonObject.toString());
				stringEntity = new StringEntity(jsonObject.toString());
				restResponse = executePostRequest(user, sb.toString(), stringEntity);
			} catch (Exception e) {
				TimeUtil.handleError(e);
			}
		}
		return restResponse;
	}

	private Worklog convertJsonToWorklog(JSONObject obj, String key, String summary) throws JSONException {
		Worklog worklog = new Worklog();
		worklog.setIssueKey(key);
		worklog.setSummary(summary);
		String comment = obj.getString("comment");
		if (comment != null) {
			worklog.setComment(comment);
		}
		Integer timespent = obj.getInt("timeSpent");
		worklog.setWorked(TimeUtil.convertSecondsToTime(timespent));
		Long startDate = obj.getLong("startDate");
		worklog.setStartDateTime(TimeUtil.convertUnixTimestampToDate(startDate));
		return worklog;
	}

}
