/**
 * 
 */
package br.com.rdigit.deskjira.business;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.rdigit.deskjira.entity.Issue;
import br.com.rdigit.deskjira.entity.IssueType;
import br.com.rdigit.deskjira.entity.RestResponse;
import br.com.rdigit.deskjira.entity.User;

/**
 * @author leopoldo.barreiro
 * 
 */
@ApplicationScoped
public class IssueBusiness extends AbstractBusiness<Issue> implements IIssueBusiness {

	// private static final String ISSUES_BY_USER_QUERY =
	// "/rest/api/2/search?jql=assignee='{user}'+order+by+key+asc&fields=summary,description&maxResults=100";
	private static final String ISSUES_BY_USER_QUERY = "/rest/api/2/search?jql=assignee='{user}'+order+by+key+asc&fields=summary,description,issuetype&maxResults=100";

	private List<Issue> issues;

	@Inject
	private IUserBusiness userBusiness;

	public List<Issue> listAll(User user) {
		List<Issue> issueList = null;
		if (userBusiness.isCurrentUserValidToJira()) {
			try {
				String url = user.getJiraUrl().concat(ISSUES_BY_USER_QUERY).replace("{user}", user.getJiraUser());
				RestResponse restResponse = executeGetRequest(user, url);
				if (restResponse.getCode().equals(200)) {
					JSONObject json = null;
					json = new JSONObject(restResponse.getContent());
					Issue is;
					Integer totalIssues = json.getInt("total");
					if (totalIssues != null) {
						issueList = new ArrayList<Issue>(totalIssues);
						JSONArray jsonIssues = json.getJSONArray("issues");
						for (int i = 0; i < jsonIssues.length(); i++) {
							is = convertJsonToIssue(jsonIssues.getJSONObject(i));
							issueList.add(is);
						}
					}
				}
			} catch (Exception e) {
				log.error(e);
			}
		}
		return issueList;
	}

	private Issue convertJsonToIssue(JSONObject obj) {
		Issue issue = new Issue();
		try {
			issue.setId(obj.getLong("id"));
			issue.setIssueKey(obj.getString("key"));
			JSONObject fieldsObject = obj.getJSONObject("fields");
			String summary = fieldsObject.getString("summary");
			String description = fieldsObject.getString("description");
			IssueType issueType = convertJsonToIssueType(fieldsObject.getJSONObject("issuetype"));
			issue.setIssueType(issueType);
			if (summary != null) {
				issue.setSummary(summary);
			}
			if (description != null) {
				issue.setDescription(description);
			}
		} catch (JSONException e) {
			log.error(e);
		}
		return issue;
	}

	private IssueType convertJsonToIssueType(JSONObject obj) {
		IssueType issueType = new IssueType();
		try {
			issueType.setId(obj.getLong("id"));
			issueType.setName(obj.getString("name"));
			issueType.setIconUrl(obj.getString("iconUrl"));
		} catch (JSONException e) {
			log.error(e);
		}
		return issueType;
	}

	/**
	 * @return the issues
	 */
	public List<Issue> getIssues() {
		if (issues == null) {
			listAll(user);
		}
		return issues;
	}

	/**
	 * @param issues
	 *            the issues to set
	 */
	public void setIssues(List<Issue> issues) {
		this.issues = issues;
	}

}
