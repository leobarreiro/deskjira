/**
 * 
 */
package br.com.rdigit.deskjira.business;

import javax.enterprise.context.ApplicationScoped;

import br.com.rdigit.deskjira.entity.Issue;

/**
 * @author leopoldo.barreiro
 * 
 */
@ApplicationScoped
public interface IIssueBusiness extends IBusiness<Issue> {

}
