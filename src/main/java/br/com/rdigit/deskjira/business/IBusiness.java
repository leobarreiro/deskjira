/**
 * (c)2012 EFaber Consultoria Ltda.
 * Direitos reservados
 */
package br.com.rdigit.deskjira.business;

import java.util.List;

import org.apache.http.entity.StringEntity;

import br.com.rdigit.deskjira.entity.IEntity;
import br.com.rdigit.deskjira.entity.RestResponse;
import br.com.rdigit.deskjira.entity.User;

/**
 * @author leopoldo.barreiro
 * @since 11/11/2012 17:28:47
 */
public interface IBusiness<T extends IEntity> {

	public enum HttpMethod {
		POST, GET, PUT, DELETE
	}

	T getById(Class<T> clazz, Long id);

	void save(T entity);

	void delete(T entity);

	List<T> searchByJpql(String jpql);

	List<T> listAll(User user);

	RestResponse executeGetRequest(User user, String url);

	RestResponse executePostRequest(User user, String url, StringEntity stringEntity);

	byte[] getBinaryContent(User user, String url);

}
