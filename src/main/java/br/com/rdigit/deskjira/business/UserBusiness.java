/**
 * (c)2012 EFaber Consultoria Ltda.
 * Direitos reservados
 */
package br.com.rdigit.deskjira.business;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.Query;

import org.apache.commons.codec.binary.Base64;
import org.jboss.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.rdigit.deskjira.entity.RestResponse;
import br.com.rdigit.deskjira.entity.User;
import br.com.rdigit.deskjira.util.TimeUtil;

/**
 * @author leopoldo.barreiro
 * @since 11/11/2012 18:20:18
 */
@ApplicationScoped
public class UserBusiness extends AbstractBusiness<User> implements IUserBusiness {

	public UserBusiness() {
		super();
		log = Logger.getLogger(this.getClass());
	}

	public List<User> listAll(User user) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.inf.efaber.deskjira.business.IUserBusiness#getUserByUserName(java.
	 * lang.String)
	 */
	public User getUserByUserName(String userName) {
		Query query = entityManager.createQuery("SELECT u FROM User u WHERE u.username = :usernameSearch");
		query.setParameter("usernameSearch", userName);
		User user = null;
		@SuppressWarnings("unchecked")
		List<User> userList = (List<User>) query.getResultList();
		if (userList != null && userList.size() > 0) {
			user = userList.get(0);
		}
		return user;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.inf.efaber.deskjira.business.IUserBusiness#isCurrentUserValidToJira()
	 */
	public Boolean isCurrentUserValidToJira() {
		// User user = getCurrentUser();
		return (user.getJiraUrl() != null && !user.getJiraUrl().isEmpty() && user.getJiraUser() != null && !user.getJiraUser().isEmpty()
				&& user.getJiraPassword() != null && !user.getJiraPassword().isEmpty());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.inf.efaber.deskjira.business.IUserBusiness#isUserValidToJira(br.inf
	 * .efaber.deskjira.entity.User)
	 */
	public Boolean isUserValidToJira(User user) {
		return (user.getJiraUrl() != null && !user.getJiraUrl().isEmpty() && user.getJiraUser() != null && !user.getJiraUser().isEmpty()
				&& user.getJiraPassword() != null && !user.getJiraPassword().isEmpty());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.inf.efaber.deskjira.business.IUserBusiness#getLanguageByLabel(java
	 * .lang.String)
	 */
	public String getLanguageByLabel(String label) {
		String language = null;
		for (Language lang : Language.values()) {
			if (label.contains(lang.toString())) {
				language = lang.toString();
			}
		}
		return language;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.inf.efaber.deskjira.business.AbstractBusiness#save(br.inf.efaber.deskjira
	 * .entity.IEntity)
	 */
	@Override
	public void save(User user) {
		try {
			String passwd = new String(Base64.encodeBase64(user.getJiraPassword().getBytes()));
			user.setJiraPassword(passwd);
			if (isUserValidToJira(user)) {
				byte[] avatar = getUserIcon(user, AvatarSize.XSMALL);
				if (avatar != null) {
					user.setAvatar(avatar);
				}
			}
		} catch (Exception e1) {
			log.error(e1);
		}
		try {
			entityManager.getTransaction().begin();
			if (user.getId() == null) {
				entityManager.persist(user);
			} else {
				entityManager.merge(user);
			}
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			TimeUtil.handleError(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.inf.efaber.deskjira.business.IUserBusiness#getUserIcon(br.inf.efaber
	 * .deskjira.entity.User,
	 * br.inf.efaber.deskjira.business.IUserBusiness.AvatarSize)
	 */
	public byte[] getUserIcon(User user, AvatarSize avatarSize) {
		JSONObject json;
		byte[] bytes = null;
		StringBuilder sb = getUserJiraUrl(user);
		RestResponse restResponse = executeGetRequest(user, sb.toString());
		if (restResponse.getCode().equals(200)) {
			try {
				json = new JSONObject(restResponse.getContent());
				JSONObject jsonAvatars = json.getJSONObject("avatarUrls");
				String urlAvatar = jsonAvatars.get(avatarSize.size).toString();
				bytes = getBinaryContent(user, urlAvatar);
			} catch (JSONException e) {
				log.error(e);
			}
		}
		return bytes;
	}

	private StringBuilder getUserJiraUrl(User user) {
		StringBuilder sb = new StringBuilder();
		sb.append(user.getJiraUrl());
		sb.append("/rest/api/2/user?username=");
		sb.append(user.getJiraUser());
		return sb;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.inf.efaber.deskjira.business.IUserBusiness#isUserVerifiedInJira(br
	 * .inf.efaber.deskjira.entity.User)
	 */
	public boolean isUserVerifiedInJira(User user) {
		if (!isUserValidToJira(user)) {
			log.error("This user is not valid to connect in Jira. URL: " + user.getJiraUrl() + " . User: " + user.getJiraUser());
			return false;
		}
		StringBuilder sb = new StringBuilder();
		sb.append(user.getJiraUrl());
		RestResponse restResponse = executeGetRequest(user, getUserJiraUrl(user).toString());
		if (restResponse.getCode().equals(200)) {
			return true;
		}
		return false;
	}

}
