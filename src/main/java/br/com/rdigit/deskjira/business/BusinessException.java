/**
 * 
 */
package br.com.rdigit.deskjira.business;

import br.com.rdigit.deskjira.entity.RestResponse;

/**
 * @author leopoldo.barreiro
 *
 */
public class BusinessException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1327063518674021069L;
	private RestResponse restResponse;

	public BusinessException(RestResponse restResponse) {
		super();
		this.restResponse = restResponse;
	}

	/**
	 * @return the restResponse
	 */
	public RestResponse getRestResponse() {
		return restResponse;
	}

	/**
	 * @param restResponse the restResponse to set
	 */
	public void setRestResponse(RestResponse restResponse) {
		this.restResponse = restResponse;
	}

}
