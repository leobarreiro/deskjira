package br.com.rdigit.deskjira.business;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpMessage;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jboss.logging.Logger;

import br.com.rdigit.deskjira.entity.IEntity;
import br.com.rdigit.deskjira.entity.RestResponse;
import br.com.rdigit.deskjira.entity.User;
import br.com.rdigit.deskjira.util.TimeUtil;

/**
 * @author leopoldo.barreiro
 * @since 11/11/2012 17:33:04
 */
public abstract class AbstractBusiness<T extends IEntity> implements IBusiness<T> {

	public static String ACCEPT = "Accept";
	public static String CONTENT_TYPE = "Content-Type";
	public static String JSON_APPLICATION = "application/json; charset=ISO-8859-1";

	@Inject
	protected EntityManager entityManager;

	@Inject
	protected IUserBusiness userBusiness;

	@Inject
	@Named("currentUser")
	protected User user;

	protected Logger log;

	public T getById(Class<T> clazz, Long id) {
		T entity = entityManager.find(clazz, id);
		return entity;
	}

	public void save(T entity) {
		try {
			entityManager.getTransaction().begin();
			if (entity.getId() == null) {
				entityManager.persist(entity);
			} else {
				entityManager.merge(entity);
			}
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			TimeUtil.handleError(e);
		}
	}

	public void delete(T entity) {
		entityManager.getTransaction().begin();
		entityManager.remove(entity);
		entityManager.flush();
		entityManager.getTransaction().commit();
	}

	@SuppressWarnings("unchecked")
	public List<T> searchByJpql(String jpql) {
		List<T> results = null;
		Query query = entityManager.createQuery(jpql);
		try {
			results = (List<T>) query.getResultList();
		} catch (Exception e) {
			results = new ArrayList<T>(0);
		}
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.inf.efaber.deskjira.business.IBusiness#executeGetRequest(java.lang
	 * .String)
	 */
	public RestResponse executeGetRequest(User user, String url) {
		StringBuffer content = null;
		DefaultHttpClient httpClient = new DefaultHttpClient();
		RestResponse restResponse = new RestResponse();
		try {
			content = new StringBuffer("");
			HttpGet httpGet = new HttpGet(url);
			prepareAuthorization(httpGet, user);
			HttpResponse response = httpClient.execute(httpGet);
			restResponse.setCode(response.getStatusLine().getStatusCode());
			restResponse.setReason(response.getStatusLine().getReasonPhrase());
			BufferedReader buffreader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String input = "";
			while ((input = buffreader.readLine()) != null) {
				content.append(input);
			}
			restResponse.setContent(content.toString());
		} catch (Exception e) {
			log.error(e);
		} finally {
			httpClient.getConnectionManager().shutdown();
		}
		// log.info(url);
		return restResponse;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.inf.efaber.deskjira.business.IBusiness#executePostRequest(java.lang
	 * .String, org.apache.http.entity.StringEntity)
	 */
	public RestResponse executePostRequest(User user, String url, StringEntity stringEntity) {
		StringBuffer content = null;
		DefaultHttpClient httpClient = new DefaultHttpClient();
		RestResponse restResponse = new RestResponse();
		try {
			content = new StringBuffer("");
			HttpPost httpPost = new HttpPost(url);
			httpPost.setHeader(ACCEPT, JSON_APPLICATION);
			httpPost.setHeader(CONTENT_TYPE, JSON_APPLICATION);
			prepareAuthorization(httpPost, user);
			httpPost.setEntity(stringEntity);
			HttpResponse response = httpClient.execute(httpPost);
			restResponse.setCode(response.getStatusLine().getStatusCode());
			restResponse.setReason(response.getStatusLine().getReasonPhrase());
			BufferedReader buffreader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String input = "";
			while ((input = buffreader.readLine()) != null) {
				content.append(input);
			}
			restResponse.setContent(content.toString());
		} catch (Exception e) {
			log.error(e);
		} finally {
			httpClient.getConnectionManager().shutdown();
		}
		log.info(url);
		log.info(restResponse.getCode());
		return restResponse;
	}

	/**
	 * Prepara o cabecalho HTTP para autenticacao basica da API REST do Jira.
	 * 
	 * @param httpMessage
	 * @param user
	 */
	private void prepareAuthorization(HttpMessage httpMessage, User user) {
		final StringBuilder sb = new StringBuilder();
		sb.append(user.getJiraUser());
		sb.append(":");
		String passwd = new String(Base64.decodeBase64(user.getJiraPassword().getBytes()));
		sb.append(passwd);
		httpMessage.addHeader("Authorization", "Basic " + Base64.encodeBase64String(sb.toString().getBytes()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.inf.efaber.deskjira.business.IBusiness#getBinaryContent(br.inf.efaber
	 * .deskjira.entity.User, java.lang.String)
	 */
	public byte[] getBinaryContent(User user, String url) {
		byte[] bytes = null;
		DefaultHttpClient httpClient = new DefaultHttpClient();
		try {
			HttpGet httpGet = new HttpGet(url);
			prepareAuthorization(httpGet, user);
			HttpResponse response = httpClient.execute(httpGet);
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				entity.writeTo(bos);
				bytes = bos.toByteArray();
			}
		} catch (ClientProtocolException e) {
			log.error(e);
		} catch (IOException e) {
			log.error(e);
		} finally {
			httpClient.getConnectionManager().shutdown();
		}
		return bytes;
	}

}
