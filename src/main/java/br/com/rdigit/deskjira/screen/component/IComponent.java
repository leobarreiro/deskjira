package br.com.rdigit.deskjira.screen.component;

import br.com.rdigit.deskjira.util.Props;


public interface IComponent {

	public void translate(Props prp);

}
