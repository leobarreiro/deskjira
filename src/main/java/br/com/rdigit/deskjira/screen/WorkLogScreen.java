/**
 * (c)2012 EFaber Consultoria Ltda.
 * Direitos reservados
 */
package br.com.rdigit.deskjira.screen;

import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.wb.swt.SWTResourceManager;
import org.jboss.logging.Logger;

import br.com.rdigit.deskjira.MenuBase;
import br.com.rdigit.deskjira.business.BusinessException;
import br.com.rdigit.deskjira.business.IIssueBusiness;
import br.com.rdigit.deskjira.business.IUserBusiness;
import br.com.rdigit.deskjira.business.IWorklogBusiness;
import br.com.rdigit.deskjira.entity.Issue;
import br.com.rdigit.deskjira.entity.Worklog;
import br.com.rdigit.deskjira.listener.MaskVerifyListener;
import br.com.rdigit.deskjira.screen.component.CrudButtons;
import br.com.rdigit.deskjira.screen.component.DateComposite;
import br.com.rdigit.deskjira.screen.component.MessageBar;
import br.com.rdigit.deskjira.screen.component.MessageBar.MessageType;
import br.com.rdigit.deskjira.util.FontUtil;
import br.com.rdigit.deskjira.util.TimeUtil;

/**
 * @author leopoldo.barreiro
 * @since 17/11/2012 17:41:12
 */
@Named
public class WorkLogScreen extends AbstractScreen {

	@Inject
	@Named("defaultDisplay")
	private static Display display;

	@Inject
	private IWorklogBusiness business;

	@Inject
	private IIssueBusiness issueBusiness;

	@Inject
	private IUserBusiness userBusiness;

	// @Inject
	// @Named("currentUser")
	// private User user;

	@Inject
	private MenuBase menuBase;

	private List<Issue> issues;
	private Worklog oldWorklog;
	private MaskVerifyListener timeListener;

	private DateComposite dateComposite;

	private Label lblHoraInicio;
	private Label lblData;
	private Label lbActivity;
	private Combo cmbIssue;
	private Group grpActivityAbstract;
	private Label lblIssueTypeIcon;
	private Button btnReload;

	private Label lbTempo;
	private Text txTempo;

	private Label lbDescricao;
	private Text txDescricao;

	private CrudButtons buttons;
	private Text txHoraInicio;

	private Button btnReport;
	private Text txtAbstract;

	public WorkLogScreen() {
		super();
		log = Logger.getLogger(this.getClass());
		oldWorklog = null;
		setSize(new Point(600, 430));
		super.composeWindowBase();
		timeListener = new MaskVerifyListener(TimeUtil.MASK_TIME);

		lblData = new Label(composite, SWT.NONE);
		lblData.setBounds(16, 53, 80, 16);
		lblData.setText("date");
		lblData.setFont(basicFont);

		dateComposite = new DateComposite(composite);
		dateComposite.setLocation(98, 46);

		setText("worklog.title");
		lblScreenTitle.setText("worklog.title");

		lbActivity = new Label(composite, SWT.NONE);
		lbActivity.setBounds(16, 116, 80, 16);
		lbActivity.setFont(basicFont);
		lbActivity.setText("worklog.activity");

		cmbIssue = new Combo(composite, SWT.BORDER);
		cmbIssue.setFont(FontUtil.getFooterFont());
		cmbIssue.setBounds(102, 111, 420, 27);
		cmbIssue.setFont(basicFont);
		cmbIssue.addModifyListener(new ModifyListener() {

			public void modifyText(ModifyEvent arg0) {
				if (issues != null) {
					updateIssueInfo();
				}
			}
		});

		btnReload = new Button(composite, SWT.NONE);
		btnReload.setBounds(528, 111, 48, 28);
		btnReload.setText("worklog.reload.issues");
		btnReload.addSelectionListener(new SelectionListener() {

			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub

			}

			public void widgetSelected(SelectionEvent arg0) {
				issues = null;
				loadIssuesCombo();
			}

		});

		grpActivityAbstract = new Group(composite, SWT.NONE);
		grpActivityAbstract.setFont(FontUtil.getFooterFont());
		grpActivityAbstract.setText("worklog.activity.abstract");
		grpActivityAbstract.setBounds(103, 145, 473, 120);

		txtAbstract = new Text(grpActivityAbstract, SWT.MULTI | SWT.WRAP | SWT.SCROLL_PAGE | SWT.V_SCROLL);
		txtAbstract.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		txtAbstract.setFont(FontUtil.getFooterFont());
		txtAbstract.setBounds(38, 21, 425, 89);
		txtAbstract.setLayoutData(new GridData(GridData.FILL_BOTH));

		lblIssueTypeIcon = new Label(grpActivityAbstract, SWT.NONE);
		lblIssueTypeIcon.setBounds(10, 21, 16, 16);

		lblHoraInicio = new Label(composite, SWT.NONE);
		lblHoraInicio.setBounds(16, 84, 80, 16);
		lblHoraInicio.setText("start.time");

		txHoraInicio = new Text(composite, SWT.BORDER);
		txHoraInicio.setBounds(101, 80, 63, 24);
		txHoraInicio.addVerifyListener(timeListener);
		txHoraInicio.setFont(basicFont);

		lbTempo = new Label(composite, SWT.NONE);
		lbTempo.setBounds(212, 84, 80, 16);
		lbTempo.setText("worklog.duration");
		lbTempo.setFont(basicFont);

		txTempo = new Text(composite, SWT.BORDER);
		txTempo.setToolTipText("worklog.hhmm");
		txTempo.setBounds(298, 81, 63, 24);
		txTempo.addVerifyListener(timeListener);
		txTempo.setFont(basicFont);
		// txTempo.addModifyListener(saveModify);

		lbDescricao = new Label(composite, SWT.NONE);
		lbDescricao.setBounds(16, 280, 80, 16);
		lbDescricao.setText("worklog.description");
		lbDescricao.setFont(basicFont);

		txDescricao = new Text(composite, SWT.BORDER | SWT.MULTI | SWT.WRAP | SWT.SCROLL_PAGE);
		txDescricao.setBounds(103, 280, 473, 48);
		txDescricao.setFont(basicFont);
		// txDescricao.addModifyListener(saveModify);

		buttons = new CrudButtons(composite);
		buttons.setLocation(266, 344);
		setCrudActions(buttons);

		buttons.setEnableDelete(Boolean.FALSE);
		msgBar = new MessageBar(this);

		addSaveModifyListenerToWidgets(buttons, new Widget[] { txTempo, txDescricao, cmbIssue, txHoraInicio });

		btnReport = new Button(composite, SWT.NONE);
		btnReport.setBounds(18, 346, 90, 28);
		btnReport.setFont(basicFont);
		btnReport.setText("worklog.report.button");

		btnReport.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				openReport();
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				openReport();
			}
		});
	}

	public void initScreen() {
		super.initScreen();
		translateComponents();
		lblData.setText(prp.get("date"));
		setText(prp.get("worklog.title"));
		lblScreenTitle.setText(prp.get("worklog.title"));
		lbActivity.setText(prp.get("worklog.activity"));
		grpActivityAbstract.setText(prp.get("worklog.activity.abstract"));
		lblHoraInicio.setText(prp.get("start.time"));
		lbTempo.setText(prp.get("worklog.duration"));
		txTempo.setToolTipText(prp.get("worklog.hhmm"));
		lbDescricao.setText(prp.get("worklog.description"));
		btnReport.setText(prp.get("worklog.report.button"));
		oldWorklog = null;
		btnReload.setText(prp.get("worklog.reload.issues"));
		loadIssuesCombo();
	}

	public void clearAction() {
		oldWorklog = null;
		// txActivity.setText("");
		txTempo.removeVerifyListener(timeListener);
		txTempo.setText("");
		txTempo.addVerifyListener(timeListener);

		txHoraInicio.removeVerifyListener(timeListener);
		txHoraInicio.setText("");
		txHoraInicio.addVerifyListener(timeListener);

		cmbIssue.select(0);
		txDescricao.setText("");
	}

	public boolean isEnabledSave() {
		// TODO: considerar cbActivity
		if (txTempo != null && txTempo.getText() != null && txTempo.getText().length() > 0 && txDescricao != null && txDescricao.getText() != null
				&& txDescricao.getText().length() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public void saveAction() {
		Worklog worklog = new Worklog();
		final Calendar cal = Calendar.getInstance();
		cal.setTime(dateComposite.getDate());
		if (txHoraInicio.getText() != null && txHoraInicio.getText().indexOf(":") > 0) {
			String[] hourParts = txHoraInicio.getText().split(":");
			cal.set(Calendar.HOUR_OF_DAY, Integer.valueOf(hourParts[0]));
			cal.set(Calendar.MINUTE, Integer.valueOf(hourParts[1]));
		}
		worklog.setStartDateTime(cal.getTime());
		worklog.setWorked(txTempo.getText());
		worklog.setComment(txDescricao.getText());
		@SuppressWarnings("unchecked")
		List<Issue> issueList = (List<Issue>) cmbIssue.getData();
		Issue selectedIssue = issueList.get(cmbIssue.getSelectionIndex());
		worklog.setIssueKey(selectedIssue.getIssueKey());
		worklog.setUser(user);
		business.save(worklog);
		try {
			business.addWorkLog(worklog);
			showMessage(MessageType.INFO, prp.get("msg.record.saved"));
		} catch (BusinessException e) {
			showMessage(MessageType.ERROR, e.getRestResponse().getReason());
		}
		clearAction();
	}

	public void loadIssuesCombo() {
		if (userBusiness.isCurrentUserValidToJira()) {
			if (issues == null) {
				issues = issueBusiness.listAll(user);
			}
			String[] issueList = new String[issues.size()];
			int selected = 0;
			int counter = 0;
			for (Issue is : issues) {
				if (is.getSummary().length() <= 60) {
					issueList[counter] = "[" + is.getIssueKey() + "] " + is.getSummary();
				} else {
					issueList[counter] = "[" + is.getIssueKey() + "] " + is.getSummary().substring(0, 55).concat(" (...)");
				}
				if (oldWorklog != null && oldWorklog.getIssueKey().equalsIgnoreCase(is.getIssueKey())) {
					selected = counter;
				}
				counter++;
			}
			if (issueList != null) {
				cmbIssue.setItems(issueList);
				cmbIssue.setData(issues);
				cmbIssue.select(selected);
				updateIssueInfo();
			}
		}
	}

	@Override
	public void screenFillContent(Object object) {
	}

	public Worklog getOldWorklog() {
		return oldWorklog;
	}

	public void setOldWorklog(Worklog oldWorklog) {
		this.oldWorklog = oldWorklog;
	}

	/** Getters and Setters */

	public boolean isEnabledDelete() {
		return (oldWorklog != null);
	}

	public DateComposite getDateComposite() {
		return dateComposite;
	}

	public void deleteAction() {
		// TODO Auto-generated method stub
	}

	public void openReport() {
		menuBase.openWorklogReportScreen();
	}

	/**
	 * @return the issues
	 */
	public List<Issue> getIssues() {
		return issues;
	}

	/**
	 * @param issues
	 *            the issues to set
	 */
	public void setIssues(List<Issue> issues) {
		this.issues = issues;
	}

	private void updateIssueInfo() {
		try {
			Issue selectedIssue = issues.get(cmbIssue.getSelectionIndex());
			txtAbstract.setText(selectedIssue.toString());
			grpActivityAbstract.setText(selectedIssue.getIssueType().getName());

			// Image imgLogo = new Image(this.getDisplay(),
			// WorkLogScreen.class.getgetResourceAsStream(ResourceUtil.WINDOW_LOGO));
		} catch (Exception e) {
			log.error(e);
		}
	}
}
