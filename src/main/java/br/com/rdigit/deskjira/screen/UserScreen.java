/**
 * (c)2013 EFaber Consultoria Ltda.
 * Direitos reservados
 */
package br.com.rdigit.deskjira.screen;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.codec.binary.Base64;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Widget;

import br.com.rdigit.deskjira.business.IUserBusiness;
import br.com.rdigit.deskjira.business.IUserBusiness.Language;
import br.com.rdigit.deskjira.entity.User;
import br.com.rdigit.deskjira.screen.component.CrudButtons;
import br.com.rdigit.deskjira.screen.component.MessageBar;
import br.com.rdigit.deskjira.screen.component.MessageBar.MessageType;
import br.com.rdigit.deskjira.util.FontUtil;
import br.com.rdigit.deskjira.util.ResourceUtil;

/**
 * @author leopoldo.barreiro
 * @since 23/12/2012 13:58:41
 */
@Named
public class UserScreen extends AbstractScreen {

	private static final String JIRA = "Jira";

	@Inject
	@Named("currentUser")
	private User user;

	@Inject
	private IUserBusiness userBusiness;

	private Label lbUsername;
	private Label lbEmail;
	private Label lbJiraUrl;
	private Label lbJiraUser;
	private Label lblPwd;
	private Label lblIdioma;

	private Text txUsername;
	private Text txEmail;
	private CrudButtons buttons;
	private Text txJiraUrl;
	private Text txJiraUser;
	private Text txJiraPwd;
	private Combo cmbIdioma;
	private Button btRefreshVerify;

	/**
	 * @param display
	 */
	public UserScreen() {
		super();
		setSize(new Point(420, 400));
		super.composeWindowBase();

		lblScreenTitle.setText("user.title");
		setText("user.title");
		authUser.setVisible(false);

		lbUsername = new Label(composite, SWT.NONE);
		lbUsername.setBounds(27, 63, 130, 16);
		lbUsername.setText("user.device");
		lbUsername.setFont(basicFont);

		txUsername = new Text(composite, SWT.BORDER);
		txUsername.setEnabled(false);
		txUsername.setEditable(false);
		txUsername.setBounds(162, 58, 224, 24);
		txUsername.setFont(basicFont);

		lbEmail = new Label(composite, SWT.NONE);
		lbEmail.setBounds(27, 96, 130, 16);
		lbEmail.setText("user.email");
		lbEmail.setFont(basicFont);

		txEmail = new Text(composite, SWT.BORDER);
		txEmail.setBounds(163, 93, 224, 24);
		txEmail.setFont(basicFont);

		lbJiraUrl = new Label(composite, SWT.NONE);
		lbJiraUrl.setBounds(29, 179, 130, 16);
		lbJiraUrl.setText("user.jiraUrl");
		lbJiraUrl.setFont(basicFont);

		lbJiraUser = new Label(composite, SWT.NONE);
		lbJiraUser.setBounds(28, 216, 130, 16);
		lbJiraUser.setText("user.jirauser");
		lbJiraUser.setFont(basicFont);

		txJiraUrl = new Text(composite, SWT.BORDER);
		txJiraUrl.setBounds(164, 175, 224, 24);
		txJiraUrl.setFont(basicFont);

		txJiraUser = new Text(composite, SWT.BORDER);
		txJiraUser.setBounds(164, 211, 180, 24);
		txJiraUser.setFont(basicFont);

		lblPwd = new Label(composite, SWT.NONE);
		lblPwd.setBounds(29, 248, 130, 16);
		lblPwd.setText("user.jirapassword");

		txJiraPwd = new Text(composite, SWT.BORDER | SWT.PASSWORD);
		txJiraPwd.setBounds(164, 244, 180, 24);

		lblIdioma = new Label(composite, SWT.NONE);
		lblIdioma.setBounds(28, 283, 129, 16);
		lblIdioma.setText("user.language");
		lblIdioma.setFont(basicFont);

		cmbIdioma = new Combo(composite, SWT.NONE);
		cmbIdioma.setBounds(164, 279, 180, 24);
		cmbIdioma.setFont(basicFont);

		Label lblSep = new Label(composite, SWT.SEPARATOR | SWT.HORIZONTAL);
		lblSep.setBounds(116, 151, 270, 2);
		lblSep.setFont(basicFont);

		buttons = new CrudButtons(composite);
		buttons.setBounds(77, 325, 310, 34);
		buttons.setEnableDelete(Boolean.FALSE);

		setCrudActions(buttons);
		addSaveModifyListenerToWidgets(buttons, new Widget[] { txEmail });

		Label lblJiraLogo = new Label(composite, SWT.NONE);
		lblJiraLogo.setBounds(26, 129, 32, 36);

		Image jiraLogo = new Image(this.getDisplay(), UserScreen.class.getResourceAsStream(ResourceUtil.JIRA_LOGO));
		lblJiraLogo.setImage(jiraLogo);

		Label lblJiraTitle = new Label(composite, SWT.NONE);
		lblJiraTitle.setBounds(70, 143, 42, 16);
		lblJiraTitle.setText(JIRA);
		lblJiraTitle.setFont(FontUtil.getTitleFont());

		Image refreshVerification = new Image(this.getDisplay(), UserScreen.class.getResourceAsStream(ResourceUtil.USER_VERIFICATION));
		btRefreshVerify = new Button(composite, SWT.NONE);
		btRefreshVerify.setBounds(350, 235, 36, 36);
		btRefreshVerify.setImage(refreshVerification);

		msgBar = new MessageBar(this);
	}

	public void initScreen() {
		super.initScreen();
		translateComponents();
		lblScreenTitle.setText(prp.get("user.title"));
		setText(prp.get("user.title"));
		lbUsername.setText(prp.get("user.device"));
		lbEmail.setText(prp.get("user.email"));
		lbJiraUrl.setText(prp.get("user.jiraUrl"));
		lbJiraUser.setText(prp.get("user.jirauser"));
		lblPwd.setText(prp.get("user.jirapassword"));
		lblIdioma.setText(prp.get("user.language"));

		if (user != null) {
			txUsername.setText(user.getUsername());
			if (user.getEmail() != null) {
				txEmail.setText(user.getEmail());
			}
			if (user.getJiraUrl() != null) {
				txJiraUrl.setText(user.getJiraUrl());
			}
			if (user.getJiraUser() != null) {
				txJiraUser.setText(user.getJiraUser());
			}
			if (user.getJiraPassword() != null) {
				byte[] passwdDecoded = Base64.decodeBase64(user.getJiraPassword().getBytes());
				txJiraPwd.setText(passwdDecoded.toString());
			}
			String[] languageLabels = Language.getLabels();
			cmbIdioma.setItems(languageLabels);
			if (user.getLanguage() != null) {
				Language languageSelected = Language.getLanguageByLang(user.getLanguage());
				for (int i = 0; i < languageLabels.length; i++) {
					if (languageSelected.label.equals(languageLabels[i])) {
						cmbIdioma.select(i);
					}
				}
			}
		}
	}

	public void clearAction() {
	}

	public boolean isEnabledSave() {
		return (txEmail.getText() != null && txEmail.getText().length() > 0);
	}

	public boolean isEnabledDelete() {
		return false;
	}

	public void saveAction() {
		User userToSave = userBusiness.getUserByUserName(txUsername.getText());
		if (userToSave == null) {
			userToSave = user;
		}
		userToSave.setEmail(txEmail.getText());
		userToSave.setJiraUrl(txJiraUrl.getText());
		userToSave.setJiraUser(txJiraUser.getText());
		Language selectedLanguage = Language.getLanguageByLabel(cmbIdioma.getText());
		userToSave.setLanguage(selectedLanguage.lang);
		userToSave.setJiraPassword(new String(Base64.encodeBase64(txJiraPwd.getText().getBytes())));
		if (userBusiness.isUserVerifiedInJira(user)) {
			userToSave.setJiraPassword(txJiraPwd.getText());
			userBusiness.save(userToSave);
			showMessage(MessageType.INFO, prp.get("msg.record.saved"));
		} else {
			showMessage(MessageType.ERROR, prp.get("msg.user.not.verified.in.jira"));
		}
	}

	public void deleteAction() {
	}

	@Override
	public void screenFillContent(Object object) {
		// TODO Auto-generated method stub
	}
}
