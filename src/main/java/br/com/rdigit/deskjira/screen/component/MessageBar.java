package br.com.rdigit.deskjira.screen.component;

import javax.inject.Named;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import br.com.rdigit.deskjira.util.FontUtil;
import br.com.rdigit.deskjira.util.Props;

@Named
public class MessageBar extends Shell implements IComponent {

	private static final int WIDTH = 300;
	private static final int HEIGHT = 160;
	private static String LABEL_INFO = "";
	private static String LABEL_WARNING = "";
	private static String LABEL_ERROR = "";

	private Shell parent;
	private Composite composite;
	private Label title;
	private Label message;
	private Label icon;
	private Image imageInfo;
	private Image imageWarning;
	private Image imageError;
	private Button btnOk;

	public static final String ICON_INFO = "/img/info.png";
	public static final String ICON_WARNING = "/img/warning.png";
	public static final String ICON_ERROR = "/img/error.png";

	public enum MessageType {
		INFO, WARNING, ERROR;
	}

	public MessageBar(Shell parent) {
		super(parent, SWT.APPLICATION_MODAL);
		this.parent = parent;
		composite = new Composite(this, SWT.BORDER);
		composite.setLocation(5, 5);
		composite.setSize(WIDTH - 10, HEIGHT - 10);
		setSize(WIDTH, HEIGHT);

		title = new Label(composite, SWT.NONE);
		title.setBounds(10, 14, 230, 30);
		title.setFont(FontUtil.getDialogTitleFont());
		title.setText("label.info");

		message = new Label(composite, SWT.NONE | SWT.WRAP);
		message.setFont(FontUtil.getFooterFont());
		message.setAlignment(SWT.LEFT);
		message.setSize(268, 42);
		message.setLocation(10, 57);
		message.setText("label.info");

		icon = new Label(composite, SWT.NONE);
		icon.setSize(32, 32);
		icon.setLocation(246, 10);

		imageInfo = new Image(getDisplay(), MessageBar.class.getResourceAsStream(ICON_INFO));
		icon.setImage(imageInfo);

		btnOk = new Button(composite, SWT.NONE);
		btnOk.setBounds(102, 110, 85, 28);
		btnOk.setFont(FontUtil.getBasicFont());
		btnOk.setText("button.close");

		btnOk.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				setVisible(false);
				clear();
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
			}
		});
		imageWarning = new Image(getDisplay(), MessageBar.class.getResourceAsStream(ICON_WARNING));
		imageError = new Image(getDisplay(), MessageBar.class.getResourceAsStream(ICON_ERROR));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.inf.efaber.deskjira.screen.component.IComponent#translate()
	 */
	public void translate(Props prp) {
		LABEL_INFO = prp.get("label.info");
		LABEL_WARNING = prp.get("label.warning");
		LABEL_ERROR = prp.get("label.error");
		title.setText(LABEL_INFO);
		btnOk.setText(prp.get("button.close"));
	}

	private void setStarted(String msg) {
		Point parentLocation = parent.getLocation();
		Point parentSize = parent.getSize();
		Integer horizontalMiddle = parentLocation.x + (parentSize.x / 2);
		Integer verticalMiddle = parentLocation.y + (parentSize.y / 2);
		setLocation((horizontalMiddle - (WIDTH / 2)), (verticalMiddle - (HEIGHT / 2)));
		open();
		layout();
		message.setText(msg);
		setVisible(true);
	}

	public void showInfo(String msg) {
		title.setText(LABEL_INFO);
		icon.setImage(imageInfo);
		setStarted(msg);
	}

	public void showWarning(String msg) {
		title.setText(LABEL_WARNING);
		icon.setImage(imageWarning);
		setStarted(msg);
	}

	public void showError(String msg) {
		title.setText(LABEL_ERROR);
		icon.setImage(imageError);
		setStarted(msg);
	}

	public void clear() {
		message.setText("");
		icon.setImage(imageInfo);
	}

	@Override
	protected void checkSubclass() {
	}

	public Composite getComposite() {
		return composite;
	}

	public void setComposite(Composite composite) {
		this.composite = composite;
	}

	public Label getMessage() {
		return message;
	}

	public void setMessage(Label message) {
		this.message = message;
	}

	public Label getIcon() {
		return icon;
	}

	public void setIcon(Label icon) {
		this.icon = icon;
	}

	public Image getImageInfo() {
		return imageInfo;
	}

	public void setImageInfo(Image imageInfo) {
		this.imageInfo = imageInfo;
	}

	public Image getImageWarning() {
		return imageWarning;
	}

	public void setImageWarning(Image imageWarning) {
		this.imageWarning = imageWarning;
	}

	public Image getImageError() {
		return imageError;
	}

	public void setImageError(Image imageError) {
		this.imageError = imageError;
	}

}
