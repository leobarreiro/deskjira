package br.com.rdigit.deskjira.screen;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.events.ShellListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Widget;
import org.jboss.logging.Logger;

import br.com.rdigit.deskjira.business.IUserBusiness;
import br.com.rdigit.deskjira.entity.User;
import br.com.rdigit.deskjira.screen.component.AuthenticatedUser;
import br.com.rdigit.deskjira.screen.component.CrudButtons;
import br.com.rdigit.deskjira.screen.component.IComponent;
import br.com.rdigit.deskjira.screen.component.MessageBar;
import br.com.rdigit.deskjira.screen.component.CrudButtons.ButtonType;
import br.com.rdigit.deskjira.screen.component.MessageBar.MessageType;
import br.com.rdigit.deskjira.util.FontUtil;
import br.com.rdigit.deskjira.util.Props;
import br.com.rdigit.deskjira.util.ResourceUtil;

public abstract class AbstractScreen extends Shell implements ICrudScreen {

	protected Composite composite;
	protected Label lblScreenTitle;
	protected Label lblLogotipo;
	protected Font basicFont;
	protected Font titleFont;
	protected Font footerFont;

	protected Display display;
	protected MessageBar msgBar;

	protected AuthenticatedUser authUser;
	protected Logger log;

	@Inject
	protected Props prp;

	@Inject
	@Named("currentUser")
	protected User user;

	@Inject
	private IUserBusiness userBusiness;

	public AbstractScreen() {
		super(Display.getDefault(), SWT.MIN | SWT.CLOSE | SWT.TITLE);
		display = Display.getDefault();
		// setSize(windowSize);
		titleFont = FontUtil.getTitleFont();
		basicFont = FontUtil.getBasicFont();
		footerFont = FontUtil.getFooterFont();

		composite = new Composite(this, SWT.BORDER);
		lblScreenTitle = new Label(composite, SWT.NONE);
		lblLogotipo = new Label(composite, SWT.NONE);
		lblScreenTitle.setFont(titleFont);
		authUser = new AuthenticatedUser(composite);
		Image imgLogo = new Image(this.getDisplay(), AbstractScreen.class.getResourceAsStream(ResourceUtil.WINDOW_LOGO));
		lblLogotipo.setImage(imgLogo);

		addShellListener(new ShellListener() {

			public void shellIconified(ShellEvent arg0) {
			}

			public void shellDeiconified(ShellEvent arg0) {
			}

			public void shellDeactivated(ShellEvent arg0) {
			}

			public void shellClosed(ShellEvent arg0) {
				arg0.doit = false;
				clearAction();
				setVisible(Boolean.FALSE);
			}

			public void shellActivated(ShellEvent arg0) {
				// int frameX = getSize().x - getClientArea().width;
				// int frameY = getSize().y - getClientArea().height;
				// setSize(getSize().x + frameX, getSize().y + frameY);
			}
		});

	}

	public void composeWindowBase() {
		int width = getSize().x;
		int height = getSize().y;
		int heightResize = 0;
		String osName = System.getProperty("os.name").toLowerCase();
		if (osName.equals("linux")) {
			heightResize = 34;
		} else {
			heightResize = 24;
		}
		composite.setBounds(6, 6, (width - 12), (height - heightResize));
		lblScreenTitle.setBounds(20, 10, (width - 110), 30);
		lblLogotipo.setBounds((width - 60), 4, 40, 41);
		authUser.setLocation((width - 250), 10);

		Monitor primary = display.getPrimaryMonitor();
		Rectangle bounds = primary.getBounds();
		Rectangle rect = getBounds();
		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;
		this.setLocation(x, y);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.inf.efaber.deskjira.screen.ICrudScreen#initScreen()
	 */
	public void initScreen() {
		if (user != null && user.getAvatar() != null) {
			Image userIcon = new Image(display, new ByteArrayInputStream(user.getAvatar()));
			authUser.setUser(user);
			authUser.setIcon(userIcon);
		}
	}

	public void translateComponents() {
		if (msgBar != null) {
			msgBar.translate(prp);
		}
		List<Widget> widgetList = new ArrayList<Widget>();
		searchSubcomponentsToTranslate(this, widgetList);
		for (Widget wi : widgetList) {
			((IComponent) wi).translate(prp);
		}
	}

	private void searchSubcomponentsToTranslate(Widget widget, List<Widget> components) {
		Widget[] widgets;
		if ((widget instanceof Composite) || (widget instanceof Group)) {
			if (widget instanceof Group) {
				widgets = ((Group) widget).getChildren();
			} else {
				widgets = ((Composite) widget).getChildren();
			}
			for (Widget wi : widgets) {
				if (wi instanceof IComponent) {
					components.add(wi);
				}
				searchSubcomponentsToTranslate(wi, components);
			}
		}
	}

	public void cancelAction() {
		clearAction();
		setVisible(Boolean.FALSE);
	}

	@Override
	protected void checkSubclass() {
	}

	protected void setCrudActions(CrudButtons buttons) {
		buttons.addControl(ButtonType.SAVE, new SelectionListener() {
			public void widgetSelected(SelectionEvent arg0) {
				saveAction();
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				saveAction();
			}
		});
		buttons.addControl(ButtonType.CLEAR, new SelectionListener() {
			public void widgetSelected(SelectionEvent arg0) {
				clearAction();
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				clearAction();
			}
		});
		buttons.addControl(ButtonType.DELETE, new SelectionListener() {
			public void widgetSelected(SelectionEvent arg0) {
				deleteAction();
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				deleteAction();
			}
		});
		buttons.addControl(ButtonType.CLOSE, new SelectionListener() {
			public void widgetSelected(SelectionEvent arg0) {
				cancelAction();
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				cancelAction();
			}
		});
	}

	protected void addSaveModifyListenerToWidgets(final CrudButtons buttons, Widget[] widgets) {
		ModifyListener saveEnableModify = new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				buttons.setEnableSave(isEnabledSave());
			}
		};
		for (int i = 0; i < widgets.length; i++) {
			if (widgets[i].getClass().equals(Text.class)) {
				((Text) widgets[i]).addModifyListener(saveEnableModify);
			} else if (widgets[i].getClass().equals(Combo.class)) {
				((Combo) widgets[i]).addModifyListener(saveEnableModify);
			}
		}
	}

	protected void addSaveSelectionListenerToWidgets(final CrudButtons buttons, Widget[] widgets) {
		SelectionListener saveEnableSelection = new SelectionListener() {
			public void widgetSelected(SelectionEvent arg0) {
				buttons.setEnableSave(isEnabledSave());
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				buttons.setEnableSave(isEnabledSave());
			}
		};

		for (int i = 0; i < widgets.length; i++) {
			if (widgets[i].getClass().equals(Button.class)) {
				((Button) widgets[i]).addSelectionListener(saveEnableSelection);
			}
		}
	}

	protected void addDeleteModifyListenerToWidgets(final CrudButtons buttons, Widget[] widgets) {
		ModifyListener deleteEnableModify = new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				buttons.setEnableDelete(isEnabledSave());
			}
		};
		for (int i = 0; i < widgets.length; i++) {
			if (widgets[i].getClass().equals(Text.class)) {
				((Text) widgets[i]).addModifyListener(deleteEnableModify);
			} else if (widgets[i].getClass().equals(Combo.class)) {
				((Combo) widgets[i]).addModifyListener(deleteEnableModify);
			}
		}
	}

	protected void addDeleteSelectionListenerToWidgets(final CrudButtons buttons, Widget[] widgets) {
		SelectionListener deleteEnableSelection = new SelectionListener() {
			public void widgetSelected(SelectionEvent arg0) {
				buttons.setEnableDelete(isEnabledDelete());
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				buttons.setEnableDelete(isEnabledDelete());
			}
		};

		for (int i = 0; i < widgets.length; i++) {
			if (widgets[i].getClass().equals(Table.class)) {
				((Table) widgets[i]).addSelectionListener(deleteEnableSelection);
			} else if (widgets[i].getClass().equals(Button.class)) {
				((Button) widgets[i]).addSelectionListener(deleteEnableSelection);
			}
		}
	}

	/**
	 * Metodo usado para preencher o conteudo da tela
	 * 
	 * @param object
	 */
	public abstract void screenFillContent(Object object);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.inf.efaber.deskjira.screen.ICrudScreen#showMessage(br.inf.efaber.deskjira
	 * .screen.component.MessageBar.MessageType, java.lang.String)
	 */
	public void showMessage(MessageType messageType, String message) {
		if (messageType.equals(MessageType.INFO)) {
			msgBar.showInfo(message);
		} else if (messageType.equals(MessageType.WARNING)) {
			msgBar.showWarning(message);
		} else {
			msgBar.showError(message);
		}
	}

}
