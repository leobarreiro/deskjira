/**
 * (c)2013 EFaber Consultoria Ltda.
 * Direitos reservados
 */
package br.com.rdigit.deskjira.screen;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import br.com.rdigit.deskjira.MenuBase;
import br.com.rdigit.deskjira.business.BusinessException;
import br.com.rdigit.deskjira.business.IUserBusiness;
import br.com.rdigit.deskjira.business.IWorklogBusiness;
import br.com.rdigit.deskjira.entity.Worklog;
import br.com.rdigit.deskjira.screen.component.DateComposite;
import br.com.rdigit.deskjira.screen.component.MessageBar;
import br.com.rdigit.deskjira.screen.component.SearchButtons;
import br.com.rdigit.deskjira.screen.component.MessageBar.MessageType;
import br.com.rdigit.deskjira.screen.component.SearchButtons.ButtonType;
import br.com.rdigit.deskjira.util.TimeUtil;

/**
 * @author leopoldo.barreiro
 * @since 31/01/2013 23:38:08
 */
@Named
public class WorkLogReportScreen extends AbstractScreen {

	@Inject
	@Named("defaultDisplay")
	private static Display display;

	// @Inject
	// @Named("currentUser")
	// private User user;

	private DateComposite startDateComposite;
	private DateComposite endDateComposite;
	private Table table;
	private TableColumn tblClDate;
	private TableColumn tblClIssue;
	private TableColumn tblClSpent;
	private SearchButtons searchButtons;

	private Group grpFiltroDaPesquisa;
	private Label lblDataInicial;
	private Label lblDataFinal;
	private Label lblUser;

	@Inject
	private IWorklogBusiness worklogBusiness;

	@Inject
	private IUserBusiness userBusiness;

	@Inject
	private MenuBase menuBase;

	private Label lblUserJira;
	private Label lblTotalHorasApontadas;
	private Label lblHorasApontadas;

	private List<Worklog> worklogs;
	private TableColumn tblClKey;
	private Button btnNewWorklog;

	public WorkLogReportScreen() {
		super();
		setSize(new Point(740, 520));
		lblScreenTitle.setText("worklog.report.title");
		setText("worklog.report.title");
		super.composeWindowBase();

		grpFiltroDaPesquisa = new Group(composite, SWT.NONE);
		grpFiltroDaPesquisa.setText("search.filter");
		grpFiltroDaPesquisa.setBounds(10, 49, 706, 105);
		grpFiltroDaPesquisa.setFont(footerFont);

		lblDataInicial = new Label(grpFiltroDaPesquisa, SWT.NONE);
		lblDataInicial.setBounds(10, 35, 90, 16);
		lblDataInicial.setText("filter.start.date");
		lblDataInicial.setFont(basicFont);

		startDateComposite = new DateComposite(grpFiltroDaPesquisa);
		startDateComposite.setBounds(100, 28, 250, 30);

		lblDataFinal = new Label(grpFiltroDaPesquisa, SWT.NONE);
		lblDataFinal.setBounds(10, 68, 90, 16);
		lblDataFinal.setText("filter.end.date");
		lblDataFinal.setFont(basicFont);

		endDateComposite = new DateComposite(grpFiltroDaPesquisa);
		endDateComposite.setBounds(100, 61, 248, 29);

		searchButtons = new SearchButtons(grpFiltroDaPesquisa);
		searchButtons.setBounds(448, 60, 250, 30);

		searchButtons.addControl(ButtonType.SEARCH, new SelectionListener() {
			public void widgetSelected(SelectionEvent arg0) {
				doAction();
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				doAction();
			}

			private void doAction() {
				searchAction();
			}
		});

		searchButtons.addControl(ButtonType.CLEAR, new SelectionListener() {
			public void widgetSelected(SelectionEvent arg0) {
				doAction();
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				doAction();
			}

			public void doAction() {
				clearAction();
			}
		});

		searchButtons.addControl(ButtonType.CLOSE, new SelectionListener() {
			public void widgetSelected(SelectionEvent arg0) {
				doAction();
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				doAction();
			}

			private void doAction() {
				cancelAction();
			}
		});

		msgBar = new MessageBar(this);

		btnNewWorklog = new Button(grpFiltroDaPesquisa, SWT.NONE);
		btnNewWorklog.setBounds(628, 23, 70, 28);
		btnNewWorklog.setText("label.new");
		btnNewWorklog.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent arg0) {
				openNewWorklog();
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				openNewWorklog();
			}
		});

		Label lblSep0 = new Label(composite, SWT.SEPARATOR | SWT.HORIZONTAL);
		lblSep0.setBounds(10, 157, 706, 13);

		table = new Table(composite, SWT.BORDER | SWT.FULL_SELECTION);
		table.setBounds(10, 194, 706, 282);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		// TODO Habilitar duplo clique para chamar a tela de edicao de worklog
		table.addMouseListener(new MouseListener() {
			public void mouseDoubleClick(MouseEvent event) {
				if (!worklogs.isEmpty()) {
					// worklogScreen.open();
					// worklogScreen.layout();
					// worklogScreen.initScreen();
				}
			}

			public void mouseDown(MouseEvent arg0) {
				// TODO Auto-generated method stub
			}

			public void mouseUp(MouseEvent arg0) {
				// TODO Auto-generated method stub
			}
		});

		tblClDate = new TableColumn(table, SWT.NONE);
		tblClDate.setWidth(100);
		tblClDate.setText("date");

		tblClKey = new TableColumn(table, SWT.NONE);
		tblClKey.setWidth(100);
		tblClKey.setText("worklog.issuekey");

		tblClIssue = new TableColumn(table, SWT.NONE);
		tblClIssue.setWidth(430);
		tblClIssue.setText("worklog.comment");

		tblClSpent = new TableColumn(table, SWT.NONE);
		tblClSpent.setWidth(72);
		tblClSpent.setText("worklog.time");

		lblHorasApontadas = new Label(composite, SWT.NONE);
		lblHorasApontadas.setBounds(10, 172, 180, 16);
		lblHorasApontadas.setText("worklog.report.info.total");

		lblTotalHorasApontadas = new Label(composite, SWT.NONE);
		lblTotalHorasApontadas.setAlignment(SWT.RIGHT);
		lblTotalHorasApontadas.setBounds(196, 171, 80, 16);
		lblTotalHorasApontadas.setText("worklog.hhmm");
		lblTotalHorasApontadas.setFont(basicFont);

		lblUser = new Label(composite, SWT.NONE);
		lblUser.setBounds(390, 172, 110, 16);
		lblUser.setText("user.jirauser".concat(":"));

		lblUserJira = new Label(composite, SWT.NONE);
		lblUserJira.setBounds(516, 172, 200, 16);
		lblUserJira.setText("");

		Label label = new Label(composite, SWT.SEPARATOR | SWT.VERTICAL);
		label.setAlignment(SWT.CENTER);
		label.setBounds(350, 162, 1, 30);
	}

	public void initScreen() {
		super.initScreen();
		translateComponents();
		lblScreenTitle.setText(prp.get("worklog.report.title"));
		setText(prp.get("worklog.report.title"));
		grpFiltroDaPesquisa.setText(prp.get("search.filter"));
		lblDataInicial.setText(prp.get("filter.start.date"));
		lblDataFinal.setText(prp.get("filter.end.date"));
		btnNewWorklog.setText(prp.get("label.new"));

		tblClDate.setText(prp.get("date"));
		tblClKey.setText(prp.get("worklog.issuekey"));
		tblClIssue.setText(prp.get("worklog.comment"));
		tblClSpent.setText(prp.get("worklog.time"));

		lblHorasApontadas.setText(prp.get("worklog.report.info.total"));
		lblTotalHorasApontadas.setText(prp.get("worklog.hhmm"));
		lblUser.setText(prp.get("user.jirauser").concat(":"));

		lblUserJira.setText(user.getJiraUser());
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		startDateComposite.setDate(cal.getTime());
		endDateComposite.resetComponent();
		table.clearAll();
		table.removeAll();
		lblTotalHorasApontadas.setText(prp.get("worklog.hhmm"));
	}

	public void clearAction() {
		initScreen();
	}

	public void searchAction() {
		worklogs = new ArrayList<Worklog>(0);
		table.removeAll();
		table.clearAll();
		try {
			worklogs = worklogBusiness.getWorklogsByDateInterval(user, startDateComposite.getDate(), endDateComposite
					.getDate());
		} catch (BusinessException e) {
			showMessage(MessageType.ERROR, e.getRestResponse().getReason());
		}
		Long seconds = new Long(0);
		for (Worklog w : worklogs) {
			TableItem tbItem = new TableItem(table, SWT.NONE);
			tbItem.setText(new String[] { TimeUtil.dateToString(w.getStartDateTime(), prp.get("mask.date")),
					w.getIssueKey(), w.getComment(), w.getWorked() });
			tbItem.setData(w);
			seconds += TimeUtil.convertTimeToSeconds(w.getWorked());
		}
		lblTotalHorasApontadas.setText(TimeUtil.convertSecondsToTime(Integer.valueOf(seconds.toString())));
	}

	public boolean isEnabledSave() {
		return false;
	}

	public boolean isEnabledDelete() {
		return false;
	}

	public void saveAction() {
		// TODO Auto-generated method stub
	}

	public void deleteAction() {
		// TODO Auto-generated method stub
	}

	@Override
	public void screenFillContent(Object object) {
		// TODO Auto-generated method stub

	}

	public void openNewWorklog() {
		menuBase.openWorklogScreen();
	}

}
