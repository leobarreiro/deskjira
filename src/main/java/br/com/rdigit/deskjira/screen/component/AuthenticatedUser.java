/**
 * 
 */
package br.com.rdigit.deskjira.screen.component;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import br.com.rdigit.deskjira.business.IUserBusiness.AvatarSize;
import br.com.rdigit.deskjira.entity.User;
import br.com.rdigit.deskjira.util.FontUtil;
import br.com.rdigit.deskjira.util.Props;

/**
 * @author leopoldo.barreiro
 * 
 */
public class AuthenticatedUser extends Group implements IComponent {

	private Label userName;
	private Label avatar;
	private User user;
	private Image icon;
	private AvatarSize avatarSize;

	public AuthenticatedUser(Composite arg0) {
		super(arg0, SWT.NONE);
		setSize(180, 34);
		setFont(FontUtil.getFooterFont());
		avatarSize = AvatarSize.XSMALL;
		avatar = new Label(this, SWT.NONE);
		avatar.setLocation(4, 14);
		avatar.setSize(avatarSize.width, avatarSize.height);
		userName = new Label(this, SWT.NONE);
		userName.setLocation(30, 14);
		userName.setSize(new Point(146, 16));
		userName.setFont(FontUtil.getFooterFont());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.widgets.Group#checkSubclass()
	 */
	@Override
	protected void checkSubclass() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.inf.efaber.deskjira.screen.component.IComponent#translate(br.inf.efaber
	 * .deskjira.util.Props)
	 */
	public void translate(Props prp) {
		this.setText(prp.get("user.authenticated"));
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(User user) {
		this.user = user;
		this.userName.setText(user.getJiraUser());
	}

	/**
	 * @return the icon
	 */
	public Image getIcon() {
		return icon;
	}

	/**
	 * @param icon
	 *            the icon to set
	 */
	public void setIcon(Image icon) {
		this.icon = icon;
		avatar.setImage(icon);
	}

}
