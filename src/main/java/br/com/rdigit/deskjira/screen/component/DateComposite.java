/**
 * (c)2012 EFaber Consultoria Ltda.
 * Direitos reservados
 */
package br.com.rdigit.deskjira.screen.component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Named;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import br.com.rdigit.deskjira.listener.MaskVerifyListener;
import br.com.rdigit.deskjira.util.FontUtil;
import br.com.rdigit.deskjira.util.Props;
import br.com.rdigit.deskjira.util.TimeUtil;

/**
 * @author leopoldo.barreiro
 * @since 17/11/2012 19:20:33
 */
@Named
public class DateComposite extends Composite implements IComponent {

	private SimpleDateFormat sdf;
	// private Label lbDate;
	private Text txDate;
	private Label lbDayOfWeek;
	private Button btDayBefore;
	private Button btDayAfter;
	private Button btTypeToday;
	private MaskVerifyListener dateListener;
	private Font baseFont;
	private Props prp;

	public DateComposite(final Composite composite) {
		super(composite, SWT.NONE);
		setSize(250, 30);
		sdf = new SimpleDateFormat();
		baseFont = FontUtil.getBasicFont();

		txDate = new Text(this, SWT.BORDER);
		txDate.setBounds(3, 3, 80, 24);
		txDate.setFont(baseFont);

		lbDayOfWeek = new Label(this, SWT.NONE);
		lbDayOfWeek.setBounds(90, 6, 40, 20);
		lbDayOfWeek.setFont(baseFont);

		btDayBefore = new Button(this, SWT.NONE);
		btDayBefore.setBounds(138, 3, 30, 26);
		btDayBefore.setText("button.yesterday");
		btDayBefore.setFont(baseFont);

		btDayAfter = new Button(this, SWT.NONE);
		btDayAfter.setBounds(172, 3, 30, 26);
		btDayAfter.setText("button.tomorrow");
		btDayAfter.setFont(baseFont);

		btTypeToday = new Button(this, SWT.NONE);
		btTypeToday.setBounds(208, 3, 40, 26);
		btTypeToday.setText("today.acronym");
		btTypeToday.setFont(baseFont);

		dateListener = new MaskVerifyListener(TimeUtil.DATE_MASK);
		txDate.addVerifyListener(dateListener);
		txDate.addModifyListener(new TxDateModifyListener());
		btDayBefore.addSelectionListener(new DayBeforeSelectionListener());
		btDayAfter.addSelectionListener(new DayAfterSelectionListener());
		btTypeToday.addSelectionListener(new TodaySelectionListener());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.inf.efaber.deskjira.screen.component.IComponent#translate()
	 */
	public void translate(Props prp) {
		this.prp = prp;
		btDayBefore.setText(prp.get("button.yesterday"));
		btDayAfter.setText(prp.get("button.tomorrow"));
		btTypeToday.setText(prp.get("today.acronym"));
		txDate.removeVerifyListener(dateListener);
		dateListener = new MaskVerifyListener(prp.get("mask.date"));
		txDate.addVerifyListener(dateListener);
		resetComponent();
	}

	public final void resetComponent() {
		txDate.removeVerifyListener(dateListener);
		txDate.setText(TimeUtil.actualDateString(prp.get("mask.date")));
		txDate.addVerifyListener(dateListener);
		lbDayOfWeek.setText(TimeUtil.dayOfWeek(TimeUtil.stringToDate(txDate.getText(), prp.get("mask.date")), prp));
	}

	private class TxDateModifyListener implements ModifyListener {

		public TxDateModifyListener() {
		}

		public void modifyText(ModifyEvent arg0) {
			if (txDate.getText().length() == 10) {
				sdf.applyPattern(prp.get("mask.date"));
				try {
					lbDayOfWeek.setText(TimeUtil.dayOfWeek(sdf.parse(txDate.getText()), prp));
				} catch (Exception e) {
					TimeUtil.handleError(e);
				}
			} else {
				lbDayOfWeek.setText("?");
			}
		}
	}

	private class DayBeforeSelectionListener implements SelectionListener {

		public void widgetSelected(SelectionEvent arg0) {
			doAction();
		}

		public void widgetDefaultSelected(SelectionEvent arg0) {
			doAction();
		}

		public final void doAction() {
			if (txDate.getText().length() == 10) {
				Date dt = TimeUtil.stringToDate(txDate.getText(), prp.get("mask.date"));
				Date dtBefore = TimeUtil.oneDayBefore(dt);
				txDate.removeVerifyListener(dateListener);
				txDate.setText(TimeUtil.dateToString(dtBefore, prp.get("mask.date")));
				txDate.addVerifyListener(dateListener);
			}
		}
	}

	private class DayAfterSelectionListener implements SelectionListener {

		public void widgetDefaultSelected(SelectionEvent arg0) {
			doAction();
		}

		public void widgetSelected(SelectionEvent arg0) {
			doAction();
		}

		public final void doAction() {
			if (txDate.getText().length() == 10) {
				Date dt = TimeUtil.stringToDate(txDate.getText(), prp.get("mask.date"));
				Date dtAfter = TimeUtil.oneDayAfter(dt);
				txDate.removeVerifyListener(dateListener);
				txDate.setText(TimeUtil.dateToString(dtAfter, prp.get("mask.date")));
				txDate.addVerifyListener(dateListener);
			}
		}
	}

	private class TodaySelectionListener implements SelectionListener {

		public void widgetDefaultSelected(SelectionEvent arg0) {
			doAction();
		}

		public void widgetSelected(SelectionEvent arg0) {
			doAction();
		}

		private void doAction() {
			txDate.removeVerifyListener(dateListener);
			txDate.setText(TimeUtil.actualDateString(prp.get("mask.date")));
			txDate.addVerifyListener(dateListener);
		}
	}

	public void addModifyListenerToTxDate(ModifyListener modifyListener) {
		txDate.addModifyListener(modifyListener);
	}

	public void removeModifyListenerToTxDate(ModifyListener modifyListener) {
		txDate.removeModifyListener(modifyListener);
	}

	public String getDateText() {
		return this.txDate.getText();
	}

	public Date getDate() {
		Date dt = null;
		sdf.applyPattern(prp.get("mask.date"));
		try {
			dt = sdf.parse(txDate.getText());
		} catch (ParseException e) {
			TimeUtil.handleError(e);
		}
		return dt;
	}

	public void setDate(Date date) {
		txDate.removeVerifyListener(dateListener);
		String dateContent = TimeUtil.dateToString(date, prp.get("mask.date"));
		txDate.setText(dateContent);
		txDate.addVerifyListener(dateListener);
		lbDayOfWeek.setText(TimeUtil.dayOfWeek(TimeUtil.stringToDate(txDate.getText(), prp.get("mask.date")), prp));
	}

}
