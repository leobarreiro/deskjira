/**
 * (c)2012 EFaber Consultoria Ltda.
 * Direitos reservados
 */
package br.com.rdigit.deskjira.screen.component;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import br.com.rdigit.deskjira.util.FontUtil;
import br.com.rdigit.deskjira.util.Props;

/**
 * @author leopoldo.barreiro
 * @since 24/11/2012 21:32:06
 */
@Named
public class CrudButtons extends Composite implements IComponent {

	public enum ButtonType {
		SAVE, DELETE, CLEAR, CLOSE
	}

	private Button btnSave;
	private Button btnClear;
	private Button btnDelete;
	private Button btnClose;
	private Point btnDim;
	private Font basicFont;

	@Inject
	private Props prp;

	private ModifyListener saveControlEnable;
	// TODO implementar deleteControlEnable e btnDelete
	@SuppressWarnings("unused")
	private ModifyListener deleteControlEnable;

	public CrudButtons(Composite parent) {
		super(parent, SWT.NONE);
		this.setBackground(new Color(Display.getCurrent(), 230, 230, 230));
		btnDim = new Point(70, 28);
		basicFont = FontUtil.getBasicFont();

		setVisible(Boolean.TRUE);
		setSize(((btnDim.x * 4) + 30), 34);

		btnSave = new Button(this, SWT.NONE);
		btnSave.setSize(btnDim);
		btnSave.setLocation(0, 2);
		btnSave.setEnabled(Boolean.FALSE);
		btnSave.setText("button.save");
		btnSave.setFont(basicFont);

		btnClear = new Button(this, SWT.NONE);
		btnClear.setSize(btnDim);
		btnClear.setLocation((btnDim.x + 10), 2);
		btnClear.setVisible(Boolean.TRUE);
		btnClear.setText("button.clear");
		btnClear.setFont(basicFont);

		btnDelete = new Button(this, SWT.NONE);
		btnDelete.setSize(btnDim);
		btnDelete.setLocation(((btnDim.x * 2) + 20), 2);
		btnDelete.setEnabled(Boolean.FALSE);
		btnDelete.setText("button.delete");
		btnDelete.setFont(basicFont);

		btnClose = new Button(this, SWT.NONE);
		btnClose.setSize(btnDim);
		btnClose.setLocation(((btnDim.x * 3) + 30), 2);
		btnClose.setVisible(Boolean.TRUE);
		btnClose.setText("button.close");
		btnClose.setFont(basicFont);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.inf.efaber.deskjira.screen.component.IComponent#translate()
	 */
	public void translate(Props prp) {
		btnSave.setText(prp.get("button.save"));
		btnClear.setText(prp.get("button.clear"));
		btnDelete.setText(prp.get("button.delete"));
		btnClose.setText(prp.get("button.close"));
	}

	@Override
	public void setLocation(int arg0, int arg1) {
		super.setLocation(arg0, arg1);
	}

	public ModifyListener getSaveControlEnable() {
		return this.saveControlEnable;
	}

	public void addControl(ButtonType type, SelectionListener listener) {
		if (type.equals(ButtonType.SAVE)) {
			btnSave.addSelectionListener(listener);
		} else if (type.equals(ButtonType.CLEAR)) {
			btnClear.addSelectionListener(listener);
		} else if (type.equals(ButtonType.DELETE)) {
			btnDelete.addSelectionListener(listener);
		} else {
			btnClose.addSelectionListener(listener);
		}
	}

	public void setEnableSave(boolean bo) {
		btnSave.setEnabled(bo);
	}

	public void setEnableDelete(boolean bo) {
		btnDelete.setEnabled(bo);
	}
}
