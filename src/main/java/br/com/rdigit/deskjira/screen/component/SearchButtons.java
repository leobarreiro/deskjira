/**
 * 
 */
package br.com.rdigit.deskjira.screen.component;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import br.com.rdigit.deskjira.util.FontUtil;
import br.com.rdigit.deskjira.util.Props;

/**
 * @author leopoldo.barreiro
 * 
 */
public class SearchButtons extends Composite implements IComponent {

	private Button btnSearch;
	private Button btnClear;
	private Button btnClose;
	private Point btnDim;
	private Font basicFont;

	public enum ButtonType {
		SEARCH, CLEAR, CLOSE
	}

	public SearchButtons(Composite parent) {
		super(parent, SWT.NONE);
		btnDim = new Point(70, 28);
		basicFont = FontUtil.getBasicFont();
		setVisible(Boolean.TRUE);
		setSize(((btnDim.x * 3) + 40), (btnDim.y + 4));

		btnSearch = new Button(this, SWT.NONE);
		btnSearch.setSize(btnDim);
		btnSearch.setLocation(0, 2);
		btnSearch.setEnabled(Boolean.TRUE);
		btnSearch.setText("button.search");
		btnSearch.setFont(basicFont);

		btnClear = new Button(this, SWT.NONE);
		btnClear.setSize(btnDim);
		btnClear.setLocation(90, 2);
		btnClear.setEnabled(Boolean.TRUE);
		btnClear.setText("button.clear");
		btnClear.setFont(basicFont);

		btnClose = new Button(this, SWT.NONE);
		btnClose.setSize(btnDim);
		btnClose.setLocation(180, 2);
		btnClose.setEnabled(Boolean.TRUE);
		btnClose.setText("button.close");
		btnClose.setFont(basicFont);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.inf.efaber.deskjira.screen.component.IComponent#translate(br.inf.efaber
	 * .deskjira.util.Props)
	 */
	public void translate(Props prp) {
		btnSearch.setText(prp.get("button.search"));
		btnClear.setText(prp.get("button.clear"));
		btnClose.setText(prp.get("button.close"));
	}

	public void addControl(ButtonType type, SelectionListener listener) {
		if (type.equals(ButtonType.SEARCH)) {
			btnSearch.addSelectionListener(listener);
		} else if (type.equals(ButtonType.CLEAR)) {
			btnClear.addSelectionListener(listener);
		} else {
			btnClose.addSelectionListener(listener);
		}
	}

}
