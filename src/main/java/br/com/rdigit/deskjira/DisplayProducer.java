/**
 * (c)2012 EFaber Consultoria Ltda.
 * Direitos reservados
 */
package br.com.rdigit.deskjira;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;

import org.eclipse.swt.widgets.Display;

/**
 * @author leopoldo.barreiro
 * @since 16/12/2012 23:31:30
 */
@ApplicationScoped
public class DisplayProducer {

	private Display display;

	public DisplayProducer() {
		//display = Display.getCurrent();
		display = Display.getDefault();
	}

	@Produces
	@Named("defaultDisplay")
	public Display getDisplay() {
		return display;
	}

}
