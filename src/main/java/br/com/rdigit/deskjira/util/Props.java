package br.com.rdigit.deskjira.util;

import java.io.IOException;
import java.util.Locale;
import java.util.Properties;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.rdigit.deskjira.entity.User;

/**
 * @author leopoldo.barreiro
 * @since 11/11/2012 16:33:56
 */
@Named
@ApplicationScoped
public class Props {

	@SuppressWarnings("unused")
	private static final String EN = "en";
	private static final String PT = "pt";
	private static final String ES = "es";

	private static final String ES_ES = "es-ES";
	private static final String EN_US = "en-US";
	private static final String PT_BR = "pt-BR";

	private User user;
	private Properties properties;

	public Props() {
	}

	@Inject
	public Props(@Named("currentUser") User user) {
		this.user = user;
		loadByUserPreferences();
	}

	public void loadProperties() {
		Locale locale = Locale.getDefault();
		if (locale.getLanguage().toLowerCase().equals(ES)) {
			loadPropertiesContent(ES_ES);
		} else if (locale.getLanguage().toLowerCase().equals(PT)) {
			loadPropertiesContent(PT_BR);
		} else {
			loadPropertiesContent(EN_US);
		}
	}

	public void loadByUserPreferences() {
		if (user != null && user.getLanguage() != null && !user.getLanguage().isEmpty()) {
			loadPropertiesContent(user.getLanguage());
		} else {
			loadProperties();
		}
	}

	private void loadPropertiesContent(String language) {
		StringBuilder sb = new StringBuilder();
		sb.append("/languages/");
		sb.append(language);
		sb.append(".properties");
		properties = new Properties();
		try {
			properties.load(Props.class.getResourceAsStream(sb.toString()));
		} catch (IOException e) {
			TimeUtil.handleError(e);
		}
	}

	public String get(String key) {
		if (properties.containsKey(key)) {
			return properties.getProperty(key);
		}
		return key;
	}

}
