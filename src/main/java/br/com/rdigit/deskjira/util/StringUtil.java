/**
 * 
 */
package br.com.rdigit.deskjira.util;

/**
 * @author leopoldo.barreiro
 * 
 */
public class StringUtil {

    public static String stripAccents(String originalStr) {
        String[] nativeStr = { "ç", "á", "ã", "à", "é", "ê", "ẽ", "ó", "õ", "ú" };
        String[] strippStr = { "c", "a", "a", "a", "e", "e", "e", "o", "o", "u" };
        String finalStr = originalStr;
        for (int i = 0; i < nativeStr.length; i++) {
            finalStr = finalStr.replace(nativeStr[i], strippStr[i]);
        }
        return finalStr;
    }

}
