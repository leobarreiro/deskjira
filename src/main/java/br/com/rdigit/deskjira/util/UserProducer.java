/**
 * 
 */
package br.com.rdigit.deskjira.util;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.rdigit.deskjira.business.IUserBusiness;
import br.com.rdigit.deskjira.business.IUserBusiness.AvatarSize;
import br.com.rdigit.deskjira.entity.User;

/**
 * @author leopoldo.barreiro
 * 
 */
@Named
public class UserProducer {

	@Inject
	private IUserBusiness userBusiness;

	@Produces
	@Named("currentUser")
	@ApplicationScoped
	public User getCurrentUser() {
		String username = System.getProperty("user.name");
		User user = userBusiness.getUserByUserName(username);
		if (user != null && userBusiness.isUserValidToJira(user)) {
			byte[] avatarBytes = userBusiness.getUserIcon(user, AvatarSize.XSMALL);
			if (avatarBytes != null) {
				user.setAvatar(avatarBytes);
			}
		} else {
			user = new User();
			user.setUsername(username);
			userBusiness.save(user);
		}
		return user;
	}

}
