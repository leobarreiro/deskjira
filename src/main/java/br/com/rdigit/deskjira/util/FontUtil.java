/**
 * (c)2013 EFaber Consultoria Ltda.
 * Direitos reservados
 */
package br.com.rdigit.deskjira.util;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

/**
 * @author leopoldo.barreiro
 * @since 19/01/2013 13:01:22
 */
@ApplicationScoped
@Named
public class FontUtil {

	public static final String MAIN_FONT_FAMILIY = "Arial";
	public static final Integer MAIN_FONT_SIZE = 10;
	public static final String TITLE_FONT_FAMILY = "Arial";
	public static final Integer TITLE_FONT_SIZE = 12;
	public static final Integer FOOTER_FONT_SIZE = 9;
	public static final String DIALOG_TITLE_FONT_FAMILY = "Arial";
	public static final Integer DIALOG_TITLE_FONT_SIZE = 16;

	public static Font getBasicFont() {
		Font basicFont = new Font(Display.getDefault(), MAIN_FONT_FAMILIY, MAIN_FONT_SIZE, SWT.NORMAL);
		return basicFont;
	}

	public static Font getFooterFont() {
		Font footerFont = new Font(Display.getDefault(), MAIN_FONT_FAMILIY, FOOTER_FONT_SIZE, SWT.NORMAL);
		return footerFont;
	}

	public static Font getTitleFont() {
		Font titleFont = new Font(Display.getDefault(), TITLE_FONT_FAMILY, TITLE_FONT_SIZE, SWT.BOLD);
		return titleFont;
	}

	public static Font getDialogTitleFont() {
		Font dialogTitleFont = new Font(Display.getDefault(), DIALOG_TITLE_FONT_FAMILY, DIALOG_TITLE_FONT_SIZE,
				SWT.BOLD);
		return dialogTitleFont;
	}

}
