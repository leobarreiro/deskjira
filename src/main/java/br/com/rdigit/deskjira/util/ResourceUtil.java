/**
 * 
 */
package br.com.rdigit.deskjira.util;

/**
 * @author leopoldo.barreiro
 * 
 */
public class ResourceUtil {
	public static final String WINDOW_LOGO = "/img/microefaber.png";
	public static final String JIRA_LOGO = "/img/slice-jira.png";
	public static final String USER_VERIFICATION = "/img/refresh_verify.png";

	public static final String DATA_SIGNATURE = "DATA_SIGNATURE";
	public static final String DATA_CONTENT = "MenuBaseDataContent";

	public static final String MENU_ICON_STOP = "/img/stop.png";
	public static final String MENU_ICON_PERIODS = "/img/chronometer.png";
	public static final String MENU_ICON_WORKLOGS = "/img/worklog.png";
	public static final String MENU_ICON_REPORT = "/img/reportworklog.png";
	public static final String MENU_ICON_USER_OPTS = "/img/useropt.png";
	public static final String MENU_ICON_PROJECT = "/img/project.png";
	public static final String MENU_ICON_WORKLOG_STOPED = "/img/go.png";
	public static final String MENU_ICON_WORKLOG_RUNNING = "/img/hourglass.png";
	public static final String MENU_ICON_ACTIVITY = "/img/activity.png";
	public static final String MENU_ICON_QUIT = "/img/quit.png";
	public static final String MENU_ITEM_ACTIVITY = "MENU_ITEM_ACTIVITY";
	public static final String SEPARATOR_MENU_FINISH = "MENU_FINISH";
}
