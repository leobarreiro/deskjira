deskjira
========

DeskJira is an application that brings JIRA and your desktop. This enables a tray icon on your desktop that gives access to all the features of the application. With it you can record and consult your worklogs. DeskJira is written in SWT and uses CDI, JPA2, RESTful and some other technologies in the Java platform.
